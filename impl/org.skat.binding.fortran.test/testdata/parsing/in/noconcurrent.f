Should be recognized as "unknown do" with text blob:
 do concurrent (nonsense e = 1:n)
  ! foo bar foobar
    derivative = ultranonsense(:,e) * r(:,e)

  ! calculate the temperature at foo with bar
    T_previous(:,e) = XX * (4 * T_now(:,e) - T_old(:,e) + 2 * dt * derivative)

  end do
