program main
  implicit none
  integer, dimension(10) :: a
  integer :: x

  a = (/ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 /)

  do concurrent (x = 1:10)
    ! simply set element at position x of array a to x*x
    a(x) = x * x
  end do

  do concurrent (e = 1:n_ele)
  ! calculate some stuff in inner do loop
    do i = 0, po
      f(i,e) = matrix(i) * 2 / h * Heat(arg, beta, T(i,e))
    end do

  ! add some stuff
    f(:,e) = f(:,e) - lambda * matmul(matrix, T(:,e)) * h / 10
  end do

end program main
