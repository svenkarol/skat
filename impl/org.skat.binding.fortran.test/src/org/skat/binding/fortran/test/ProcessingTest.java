/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.fortran.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.skat.binding.fortran.FPProcessor;
import org.skat.isc.core.StatusDescriptor;
import org.skat.isc.minimal.CodeGenException;
import org.skat.isc.minimal.test.TestBase;

public class ProcessingTest extends TestBase {
	
	private FPProcessor fpp = null;
	
	@Before
	public void setUp() throws Exception {
		String base = (new File("testdata/processing/doconcurrent/")).toURI().toString();
		String in = (new File("testdata/processing/doconcurrent/in/")).toURI().toString();
		String out = (new File("testdata/processing/doconcurrent/out/")).toURI().toString();
		fpp = new FPProcessor(base,in,out);
		clearOut();
	}

	
	@Test
	public void testProcessing() throws IOException {
		fpp.processMacros(null);
		if(!fpp.getCore().getEnv().status.isEmpty()){
			for(StatusDescriptor p:fpp.getCore().getEnv().status){
				System.out.println(p.getMessage());
			}
		}
		fpp.persistAll();
		compareExpected();
	}

	@Override
	public String getOutURI() {
		return  fpp.getCore().getOutURI();
	}

	@Override
	public String getInURI() {
		return fpp.getCore().getInURI();
	}

	@Override
	public String getExpectedURI() {
		return (new File("testdata/processing/doconcurrent/expected/")).toURI().toString();
	}

}
