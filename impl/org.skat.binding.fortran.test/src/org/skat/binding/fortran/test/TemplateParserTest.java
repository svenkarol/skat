  
/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.fortran.test;

import static org.junit.Assert.*;
import org.skat.binding.fortran.*;
import org.skat.binding.fortran.ast.GenericFragment;

import org.junit.Test;

import xtc.parser.ParseError;
import xtc.parser.Result;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;


import java.util.Scanner;

public class TemplateParserTest {
	
	@Test
	public void testNotAnIsland() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/noconcurrent.f";
		File file = new File(fileName);
		String content = readContent(file);
		Object result = doParse(content, fileName);
		assertTrue(result instanceof GenericFragment);
	}
	
	@Test
	public void testSomeDo() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/somedo.f";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}

	
	private void testParseAndReprint(String content, String fileName) throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		Object value = doParse(content,fileName);
		assertEquals("org.skat.binding.fortran.ast.GenericFragment",value.getClass().getName());
		System.out.println("Parsing " + fileName + ": OK");
		String source = (String) value.getClass().getMethod("genString").invoke(value);
		assertEquals(content, source);		
	}
	
	private Object doParse(String content, String fileName) throws IOException{
		StringReader reader = new StringReader(content);
		FPPParser parser = new FPPParser(reader,fileName);
		Result result = parser.pGenericFragmentDecl(0); 
		assertNotNull(result);

		if(result instanceof ParseError){
			String msg = "ERROR:"+result.parseError().msg+"("+parser.location(result.index).line+","+parser.location(result.index).column+")";
			System.out.println("Parsing " + fileName + ": "+ msg);
		}  
		assertFalse(result instanceof ParseError);
		return result.semanticValue();
		
	}
	
	private String readContent(File file) throws FileNotFoundException{
		String content = new Scanner(file).useDelimiter("\\Z").next();
		return content;
	}

}
