/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

import org.skat.binding.vtpl.VtplParser;
import org.skat.binding.vtpl.ast.ASTNode;
import org.skat.binding.vtpl.ast.Box;
import org.skat.binding.vtpl.ast.CompositionSystem;
import org.skat.binding.vtpl.ast.CompositionEnvironment;
import org.skat.binding.vtpl.ast.GenericFragment;
import org.skat.binding.vtpl.ast.GenericSource;
import org.skat.binding.vtpl.ast.Prototype;
import org.skat.binding.vtpl.ast.TextBlob;
import org.skat.binding.vtpl.ast.VariantList;
import org.skat.isc.core.StepResult;
import org.skat.isc.minimal.CodeGenException;

/**
 * Basic API implementation of the minimal API to be extended by clients. All implemented methods in this
 * class are independent of a certain AST grammar since the code for the AST and the parser is meant to be generated
 * and thus cannot have any AST-specific dependency.  
 * 
 * @author skarol
 *
 */
public class VTPLTemplate extends CompositionSystem {
		
	private String codeBlock = null;
	private String name = null;
	
	/**
	 * Constructor that creates the composition system with one code string.
	 * 
	 * @param baseURI
	 * @param code
	 * @throws IOException
	 */
	public VTPLTemplate(String baseURI, String code) throws IOException{
		super(baseURI);
		initialize(code,"<internal.file>");
	}
	

	private synchronized void initialize(String code, String name) throws CodeGenException{
		codeBlock = code;
		StringReader reader = new StringReader(code);
		this.name = name;
		VtplParser parser = new VtplParser(reader,name);
		GenericFragment fragment = parser.parseGenericFragment();
		if(getEnv().getFragmentList().getNumChild()>0)
			getEnv().getFragmentList().clear();
		getEnv().getFragmentList().add(fragment);
	}
		
	
	public String getCodeBlock() {
		return codeBlock;
	}
	
	public void setCodeBlock(String codeBlock) throws CodeGenException {
		String name = this.name;
		codeBlock = null;
		name = null;
		initialize(codeBlock,name);
	}
	
	public String getGenCode() {
		return getEnv().genString();
	}

	@Override
	protected CompositionEnvironment createEnvironment() throws IOException {
		return new GenericSource();
	}


	@Override
	public void doPersist(Box fragment) throws IOException {
		throw new RuntimeException("Persitence not supported (yet).");	
	}


	public boolean bind(String slotName, int replacement) {
		return bind(slotName,String.valueOf(replacement));
	}

	public boolean bind(String slotName, boolean replacement) {
		return bind(slotName,String.valueOf(replacement));
	}



	public boolean extractVariant(String variantListName, String variantName) {
		VariantList variants = (VariantList) getEnv().findVariants(variantListName);
		boolean atLeastOneExtracted = false;
		while(variants!=null){
			if(!variants.compose(variantName))
				break;
			atLeastOneExtracted = true;
			variants = (VariantList) getEnv().findVariants(variantListName);
		}
		return atLeastOneExtracted;
	}

	public boolean instanciatePrototype(String prototypeName, String... param) {
		Map<String,ASTNode> map = new HashMap<String, ASTNode>();
		for(int i=0;i<param.length;i++){
			map.put(param[i++], new TextBlob(param[i]));
		}
		return instanciatePrototype(prototypeName,map);
	}

	public boolean instanciatePrototype(String prototypeName, String slotName,
			String value) {
		Map<String,ASTNode> valueMap = new HashMap<String,ASTNode>();
		valueMap.put(slotName, new TextBlob(value));	
		return instanciatePrototype(prototypeName, valueMap);
	}

	public boolean instanciatePrototype(String prototypeName,
			Map<String, ASTNode> instanceValues) {
		Prototype prototype = (Prototype) getEnv().findPrototype(prototypeName);
		if(prototype!=null){
			return prototype.compose(instanceValues);
		}
		return false;
	}

	public boolean removePrototype(String prototypeName) {
		Prototype prototype = (Prototype) getEnv().findPrototype(prototypeName);
		if(prototype!=null){
			return prototype.doExtract()==StepResult.OK;
		}
		return false;
	}


/*	@Override
	public boolean hasPoints() {
		return getEnvRoot().collVariants().size()>0||getEnvRoot().collSlots().size()>0||getEnvRoot().collPrototypes().size()>0;
	}*/

	public boolean hasVariant(String variantListName) {
		return getEnv().findVariants(variantListName)!=null;
	}

	public boolean hasVariants() {
		return getEnv().collVariants().size()>0;
	}	
}
