/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.fortran;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;

import org.skat.binding.fortran.ast.ASTNode;
import org.skat.binding.fortran.ast.GenericSource;
import org.skat.binding.fortran.ast.Box;
import org.skat.binding.fortran.ast.Element;
import org.skat.binding.fortran.ast.CompositionEnvironment;
import org.skat.binding.fortran.ast.GenericFragment;
import org.skat.binding.fortran.ast.List;
import org.skat.binding.fortran.ast.CompositionSystem;

import org.skat.binding.fortran.FPPParser;

import org.skat.isc.core.StatusDescriptor;
import org.skat.isc.core.IOHelper;
import org.skat.isc.minimal.CodeGenException;

import static org.skat.isc.core.IOHelper.*;

public class FPProcessor {
	
	private CompositionSystem cSys = null;
	
	public FPProcessor(String baseURI, String inURI, String outURI) throws IOException {
		instantiateCompositionSystem(baseURI,inURI,outURI);
	}
	
	private void instantiateCompositionSystem(String baseURI, String inURI, String outURI) throws IOException{
		cSys = new CompositionSystem(baseURI,inURI,outURI){

			@Override
			protected CompositionEnvironment createEnvironment() throws IOException {
				GenericSource genSource = new GenericSource();
				Collection<File> fragmentFiles = readFiles(Collections.singletonList(new File(URI.create(getInURI()))), new String[0]);
				for(File fragmentFile:fragmentFiles){
					String content = readFile(fragmentFile);
					FPPParser parser = new FPPParser(new StringReader(content),fragmentFile.getName());
					GenericFragment fragment = parser.parseGenericFragment();
					fragment.setName(fragmentFile.getName());
					genSource.addFragment(fragment);
				}
				
				{
					InputStream in = getClass().getClassLoader().getResourceAsStream("org/skat/binding/fortran/omploop.box");
					if(in==null)
						throw new CodeGenException("Could not find built-in fragment 'omploop.box'.");
					String content = IOHelper.readStream(in);
					FPPParser parser = new FPPParser(new StringReader(content),"omploop.box");
					GenericFragment fragment = parser.parseGenericFragment();
					fragment.setName("omploop.box");
					genSource.addFragment(fragment);
				}
				
				return genSource;
			}

			@Override
			public void doPersist(Box box) throws IOException {
				GenericFragment fragment = (GenericFragment)box;
				String content = fragment.genString();
				printString(content,fragment.resourceName(),new File(URI.create(getOutURI())));
			}
			
		};
	}
		
	public CompositionSystem getCore(){
		return cSys;
	}
	
	public void persist(String regex) throws IOException{
		cSys.persistFragments(regex);
	}
	
	public void persistAll() throws IOException{
		cSys.persistFragments();
	}
	
	public void processMacros(Object model) throws CodeGenException{
		for(Box fragment:cSys.getEnv().getFragmentList()){
			System.out.println("fpp> Processing: " +fragment.getName());
			processGenericElements(((GenericFragment)fragment).getElementsList());
		}
	}
	
	private boolean processGenericElements(ASTNode<?> elements) throws CodeGenException {
		int elementCount = elements.getNumChild(); 
		int i = 0; 
		while(i<elementCount){
			ASTNode<?> current = elements.getChild(i);
			if(current.getNumChild()>0)
				processGenericElements(current);
			if(current.isComposer()){
				current.compose();
				for(StatusDescriptor p:cSys.getEnv().status){
					System.out.println(p);
				}
				elementCount = elements.getNumChild(); 
			}
			else{
				i++;
			}
		}
		return true;
	}	
}
