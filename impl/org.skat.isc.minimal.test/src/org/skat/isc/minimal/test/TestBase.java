/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */


package org.skat.isc.minimal.test;

import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URI;

import static org.skat.isc.core.IOHelper.*;

public abstract class TestBase {
		
	public abstract String getOutURI();
	public abstract String getInURI();
	public abstract String getExpectedURI();
	
	public String getEncoding(){
		return "UTF-8";
	}
	
	protected void clearOut(){
		File out = new File(URI.create(getOutURI()));
		if(out.exists()){
			File[] subFiles = out.listFiles();
			for(int i=0;i<subFiles.length;i++){
				if(!subFiles[i].getName().startsWith("."))
					subFiles[i].delete();
			}		
		}
	}
	
	/**
	 * Compare the content of expected files and actual output of the composition using the encoding currently set
	 * in the composition system. Before the composition, newline encodings of Windows and Mac are normalized to Unix. 
	 */
	protected void compareExpected(){
		File out = new File(URI.create(getOutURI()));
		File expected = new File(URI.create(getExpectedURI()));
		Assert.assertTrue(out.exists());
		Assert.assertTrue(expected.exists());
		File[] outList = out.listFiles();
		File[] expectedList = expected.listFiles();
		Assert.assertEquals(expectedList.length,outList.length);
		for(int i=0;i<expectedList.length;i++){
			if(!expectedList[i].getName().startsWith(".")){
				Assert.assertEquals(expectedList[i].getName(),outList[i].getName());
				try {
					String expectedContent = readFile(expectedList[i],getEncoding())
							.replaceAll("\\r\\n", "\n").
								replaceAll("\\r", "\n");
					String actualOut = readFile(outList[i],getEncoding())
							.replaceAll("\\r\\n", "\n").
							     replaceAll("\\r", "\n");;
					Assert.assertEquals(expectedContent,actualOut);
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				}
			}
		}

	}
	
}
