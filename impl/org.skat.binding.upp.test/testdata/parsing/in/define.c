#include "stuff.x"

#define message_for(a,b)
    printf(#a# " and " #b# ": We love you!\n")
#end

#define swap(x,y){ unsigned long _temp=#x#; x=#y#; y=_temp; }

#define swap(x,y)
    { unsigned long _temp=#x#; x=#y#; y=_temp; }
#end

int main(void)
{
   message_for(Carole, Debra);
   return 0;
}
