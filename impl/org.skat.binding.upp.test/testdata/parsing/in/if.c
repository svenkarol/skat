#include "im.port"

#if !defined (ABC)
   #define ABC "FROM ABC" #end
#else
   #define CDFG "Test" #end
#endif

int main(void)
{
   printf("Value of ABC: %s\n", ABC);
   return 0;
}
