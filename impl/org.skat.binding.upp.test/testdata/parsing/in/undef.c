#include "stuff.x"

#define message_for(a,b)
    printf(#a# " and " #b# ": We love you!\n")
#end

int main(void)
{
   message_for(Carole, Debra);
   return 0;
}

#undef message_for

#include "stuff.y"

