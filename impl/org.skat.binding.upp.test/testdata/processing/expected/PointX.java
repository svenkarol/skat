#define log(msg){ System.out.println(#msg#); }
#define THREED 

public class Point3D {

	private float x;
	private float y;
	private float z;
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		{ System.out.println("SettingX"); };
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		{ System.out.println("SettingY"); };
		this.y = y;
	}
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		{ System.out.println("SettingZ"); };
		this.z = z;
	}	
}