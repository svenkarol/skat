#define log(msg){ System.out.println(#msg#); }

public class Point2D {
	
	private float x;
	private float y;
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		{ System.out.println("SettingX"); };
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		{ System.out.println("SettingY"); };
		this.y = y;
	}
}
