#include "Point.conf"

#if defined (THREED)
public class Point3D {
#else
public class Point2D {
#endif

	private float x;
	private float y;
#ifdef THREED
	private float z;
#endif
	
	public float getX() {
		return x;
	}
	public void setX(float x) {
		log("SettingX");
		this.x = x;
	}
	public float getY() {
		return y;
	}
	public void setY(float y) {
		log("SettingY");
		this.y = y;
	}
#ifdef THREED
	public float getZ() {
		return z;
	}
	public void setZ(float z) {
		log("SettingZ");
		this.z = z;
	}	
#endif
}