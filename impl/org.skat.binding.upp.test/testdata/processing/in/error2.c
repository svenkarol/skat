#include "error.c"

#if !defined (from_im_port)
   #error "Strange things happened2"
#else
   #error "Strange things that should happen3"
#endif

#if defined (from_im_port)
   #error "Strange things that should not happen2"
#else
   #error "Strange things happened3"
#endif

#ifdef swap
	#error "Strange things happened4"
#endif

#ifndef swap
	#error "Strange things that should not happen3"
#endif

int main(void)
{
   int a = 1;
   int b = 2;
   swap(a,b);
   return 0;
}
