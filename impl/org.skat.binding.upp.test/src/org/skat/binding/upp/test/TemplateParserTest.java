/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.upp.test;
  
import static org.junit.Assert.*;

import org.junit.Test;

//Problem for AJ tooling in combination with annotations in AdaptAst.aj, we use reflection instead
//import org.jastemf.template.ast.GenericSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;


import java.util.Scanner;

public class TemplateParserTest {
	
	@Test
	public void testMacroCall() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/macrocall.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}

	
	@Test
	public void testInclude() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/include.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}

	@Test
	public void testDefine() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/define.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}
	
	@Test
	public void testIfCondition() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/if.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}
	
	@Test
	public void testUndef() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/undef.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}
	
	@Test
	public void testError() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/error.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}
	
	@Test
	public void testIfDef() throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException {		
		String fileName = "./testdata/parsing/in/ifdef.c";
		File file = new File(fileName);
		String content = readContent(file);
		testParseAndReprint(content, fileName);
	}
	
	private void testParseAndReprint(String content, String fileName) throws IOException, IllegalArgumentException, SecurityException, IllegalAccessException, InvocationTargetException, NoSuchMethodException{
		StringReader reader = new StringReader(content);
		ParserAdapter parser = new ParserAdapter(reader,fileName);
		Object result = parser.parse();
		assertNotNull(result);

		if(result instanceof ParsingError){
			System.out.println("Parsing " + fileName + ": "+ result);
		}  
		//instanceof GenericSource causes static aj compiler NullPointerException
		assertEquals("org.skat.binding.upp.ast.GenericFragment",result.getClass().getName());
		//assertTrue(result.getClass().getName().equals("org.skat.binding.upp.ast.GenericFragment"));
		System.out.println("Parsing " + fileName + ": OK");
		String source = (String) result.getClass().getMethod("genString").invoke(result);
		assertEquals(content, source);		
	}
	
	private String readContent(File file) throws FileNotFoundException{
		String content = new Scanner(file).useDelimiter("\\Z").next();
		return content;
	}

}
