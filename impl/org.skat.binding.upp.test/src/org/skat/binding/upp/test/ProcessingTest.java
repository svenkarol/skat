/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.upp.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.skat.binding.upp.UPProcessor;
import org.skat.binding.upp.ast.ASTNode;
import org.skat.binding.upp.ast.MacroCall;
import org.skat.binding.upp.ast.MacroDecl;
import org.skat.binding.upp.ast.MacroParam;
import org.skat.binding.upp.ast.PointCollector;
import org.skat.binding.upp.ast.QRef;
import org.skat.binding.upp.ast.Collector;
import org.skat.isc.core.StatusDescriptor;
import org.skat.isc.minimal.CodeGenException;
import org.skat.isc.minimal.test.TestBase;

public class ProcessingTest extends TestBase {
	
	private UPProcessor upp = null;
	
	@Before
	public void setUp() throws Exception {
		String base = (new File("testdata/processing/")).toURI().toString();
		String in = (new File("testdata/processing/in/")).toURI().toString();
		String out = (new File("testdata/processing/out/")).toURI().toString();
		upp = new UPProcessor(base,in,out);
		clearOut();
	}

	@After
	public void tearDown() throws Exception {
		//compareExpected();
	}
	

	
	@Test
	public void testJavaExample() throws IOException {
		ASTNode<?> pointNode = upp.getCore().getEnv().findPoint("log"); 
		assertNotNull(pointNode);
		assertTrue(pointNode instanceof MacroDecl);
		ASTNode<?> slotNode = upp.getCore().getEnv().findSlot("log");
		assertNotNull(slotNode);
		assertTrue(slotNode instanceof MacroCall);
		MacroDecl mDecl = (MacroDecl)pointNode;
		MacroCall mCall = (MacroCall)slotNode;
		assertFalse(mDecl.isSlot());
		assertTrue(mCall.isSlot());
		//assertTrue(mCall.parameterCheck());
		assertTrue(mCall.isBind());
		assertNull(upp.getCore().getEnv().findSlot("log2"));
		assertTrue(mDecl.getParametersList().getNumChild()==1);
		MacroParam mParam = (MacroParam)mDecl.getParametersList().getChild(0);
		assertEquals("msg",mParam.getName());
		assertEquals(1,mParam.associatedSlots().size());
	}
	
	@Test
	public void textModelCalls() throws IOException{
		ASTNode<?> pointNode = upp.getCore().getEnv().findPoint("test"); 
		assertNotNull(pointNode);
		assertTrue(pointNode instanceof MacroDecl);	
		MacroDecl mDecl = (MacroDecl)pointNode;
		List<ASTNode> testCalls = upp.getCore().getEnv().collPoints(new PointCollector(){
			protected boolean doEval(ASTNode node){ return node.isPoint() && node.hasQName(new QRef("test"));}
			public boolean cont(ASTNode node) {return true;}
		}); 
		assertEquals(7,testCalls.size());
	 	assertEquals("MacroDeclSL",testCalls.get(0).getClass().getSimpleName());
	 	for(int i=1;i<7;i++){
	 		assertTrue(testCalls.get(i) instanceof MacroCall);
			assertTrue(testCalls.get(i).isBind());	 		
	 	}
		assertFalse(testCalls.get(0).isBind());
	}
	
	public class ModelMock {
		public String foo() {return "fooValue";}
		
		public String compute(String name){
			return name;
		}
		
		public Boolean compute(boolean value){
			return !value;
		}
		
		public String compute(String name, int value, boolean truth){
			return name + value * value + truth;
		}
	}
	
	@Test
	public void testProcessing() throws IOException {
		upp.processMacros(new ModelMock());
		if(!upp.getCore().getEnv().status.isEmpty()){
			for(StatusDescriptor p:upp.getCore().getEnv().status){
				System.out.println(p.getMessage());
			}
		}
		upp.persistAll();
		compareExpected();
	}

	@Override
	public String getOutURI() {
		return  upp.getCore().getOutURI();
	}

	@Override
	public String getInURI() {
		return upp.getCore().getInURI();
	}

	@Override
	public String getExpectedURI() {
		return (new File("testdata/processing/expected/")).toURI().toString();
	}

}
