/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.java;

import java.util.Collection;
import java.util.LinkedList;
import java.io.CharArrayReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.Charset;

import static org.skat.isc.core.IOHelper.*;

import beaver.Scanner;
import org.skat.binding.java.parser.JavaScanner;
import org.skat.binding.java.parser.JavaParser;

import org.skat.binding.java.ast.ASTNode;
import org.skat.binding.java.ast.CompilationUnit;
import org.skat.binding.java.ast.CompilationUnitBox;
import org.skat.binding.java.ast.JavaFragmentBox;
import org.skat.binding.java.ast.JavaTerminalBox;
import org.skat.binding.java.ast.List;
import org.skat.binding.java.ast.Problem;
import org.skat.binding.java.ast.StringBox;
import org.skat.binding.java.ast.StringValue;

public class FragmentParser {
	
	public static class ParseException extends IOException {
		
		private static final long serialVersionUID = -5549363773634951585L;
		private Collection<Problem> problems = new LinkedList<Problem>();
		
		public ParseException(String message){
			super(message);
		}
		
		public ParseException(String message, Throwable throwable){
			super(message,throwable);
		}
		
		public ParseException(String message, Collection<Problem> problems){
			this(message);
			this.problems.addAll(problems);
		}
		
		public String getDetailedMessage(){
			String message = this.getMessage();
			message += "\nDetails:";
			for(Problem p:problems){
				message += "\n  " + "Line " + p.line() + " : " + p.message();
			}
			return message;
		}
		
		public Collection<Problem> getProblems(){
			return problems;
		}
		
	}
	
	//character used in the beaver parser as a prefix for some fragments to remove 
	//conflicts from the grammar
	public static String FRAGMENT_DELIMITER = "$ ";
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static ASTNode parseFragment(JavaScanner scanner, String name)
			throws ParseException, IOException {
		JavaParser parser = new JavaParser();
		
		ASTNode fragmentOrFragmentList = null;
		Collection<Problem> errors = parser.getErrors();
		try {
				ASTNode fragment = (ASTNode) parser.parse(scanner);
			if (fragment instanceof JavaFragmentBox) {
				fragmentOrFragmentList = fragment;
				((JavaFragmentBox)fragmentOrFragmentList).setName(name);
			} 
			else if (fragment instanceof CompilationUnit) {
					fragmentOrFragmentList = new CompilationUnitBox(name,"",
							(CompilationUnit) fragment);
					
			}
			else if(fragment instanceof List) {
				List fragmentList = (List)fragment;
				fragmentOrFragmentList = fragment;
				for(int i=0;i<fragmentList.getNumChild();i++){
					ASTNode currentFragment = fragmentList.getChild(i);
					((JavaFragmentBox)currentFragment).setName(name+"_"+i);
				}
			}

		} catch (JavaParser.Exception e) {
			// build empty compilation unit for failed error recovery
			fragmentOrFragmentList = new CompilationUnitBox();	
			errors.addAll(parser.getErrors());
			//errors.add(new Problem(null, e.getMessage(), 0, 0,
			//		Problem.Severity.ERROR, Problem.Kind.OTHER));
		} catch (Exception e) {
			fragmentOrFragmentList = new CompilationUnitBox();
			if(e instanceof Scanner.Exception){
				errors.add(new Problem(null, e.getMessage(), 0, 0,
						Problem.Severity.ERROR, Problem.Kind.LEXICAL));				
			}
			else{
				errors.add(new Problem(null, e.getMessage(), 0, 0,
						Problem.Severity.ERROR, Problem.Kind.OTHER));				
			}
		}
		if(!errors.isEmpty()){
			throw new ParseException("Problem(s) in fragment "+name+".",errors);
		}
		return fragmentOrFragmentList;
	}
	
	@SuppressWarnings("rawtypes")
	public static ASTNode parseFragment(InputStream content, String name) throws IOException, ParseException{
		JavaScanner scanner = new JavaScanner(
				new scanner.Unicode(content));
		return parseFragment(scanner,name);
	}
	
	@SuppressWarnings("rawtypes")
	public static ASTNode parseFragment(InputStream content, String name, String encoding) throws IOException, ParseException{		
		if(encoding!=null){
			JavaScanner scanner = new JavaScanner(
					new scanner.Unicode(new InputStreamReader(content, Charset.forName(encoding))));
			return parseFragment(scanner,name);
		}
		else
			return parseFragment(content,name);
		
	}
	
	
	/**
	 * Tries to parse a given fragment from the input reader. If the value of prefix = true, then
	 * in case of a failure, the parse is tried a second time using the FRAGMENT_DELIMITER. 
	 * 
	 * @param content
	 * @param name
	 * @param prefix
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */
	@SuppressWarnings("rawtypes")
	public static ASTNode parseFragment(Reader content, String name, boolean prefix) throws IOException, ParseException{
		JavaScanner scanner = new JavaScanner(
				new scanner.Unicode(content));
		try{
			return parseFragment(scanner,name);			
		}
		catch(ParseException e){
			if(prefix){
				content.reset();
				String contentString = readStream(content);
				if(!contentString.startsWith("NAME::=")){
					try{
						return parseFragment("NAME::=" + contentString,name,false);						
					}
					catch(ParseException inner){
						e.problems.add(new Problem(null, "Name parsing also failed:", 0, 0,
								Problem.Severity.WARNING, Problem.Kind.OTHER));
						e.problems.addAll(inner.problems);						
					}
					
				}
				if(!contentString.startsWith(FRAGMENT_DELIMITER)){
					try{
						return parseFragment(FRAGMENT_DELIMITER + contentString,name,false);						
					}
					catch(ParseException inner){
						e.problems.add(new Problem(null, "Prefixed parsing also failed:", 0, 0,
								Problem.Severity.WARNING, Problem.Kind.OTHER));
						e.problems.addAll(inner.problems);
					}
				}
				if(!contentString.startsWith("EXPRESSION::=")){
					try{
						return parseFragment("EXPRESSION::=" + contentString,name,false);						
					}
					catch(ParseException inner){
						e.problems.add(new Problem(null, "Expression parsing also failed:", 0, 0,
								Problem.Severity.WARNING, Problem.Kind.OTHER));
						e.problems.addAll(inner.problems);						
					}
					
				}
			}
			throw e;
		}
	}
	
	/**
	 * Tries to parse a given fragment from the input string. If the value of prefix = true, then
	 * in case of a failure, the parse is tried a second time using the FRAGMENT_DELIMITER. 
	 * 
	 * @param content
	 * @param name
	 * @param prefix
	 * @return
	 * @throws IOException
	 * @throws ParseException
	 */	
	@SuppressWarnings("rawtypes")
	public static ASTNode parseFragment(String content, String name, boolean prefix) throws IOException, ParseException{		
		CharArrayReader reader = new CharArrayReader(content.toCharArray());
		return parseFragment(reader,name, prefix);
	}
	
	public static JavaTerminalBox createTerminalFragment(Object content, String name){
		if(String.class.isInstance(content)){
			StringBox box = new StringBox();
			box.setFragment(new StringValue(content.toString()));
			box.setName(name);
			return box;
		}
		return null;	
	}
	
	

}
