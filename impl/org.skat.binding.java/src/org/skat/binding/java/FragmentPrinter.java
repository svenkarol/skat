/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.java;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.FileWriter;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;

import org.skat.binding.java.ast.Box;

public class FragmentPrinter {
	
	
	public static void printFragment(Box fragment, File outputDirectory) throws IOException {
		if(!outputDirectory.isDirectory()){
			throw new IOException("Directory '"+ outputDirectory.toString() +"' is not a directory.");
		}
		String fileName = fragment.resourceName();
		File outputFile = new File(outputDirectory, fileName);
		if(!outputFile.getParentFile().exists()){
			outputFile.getParentFile().mkdirs();
		}
		FileWriter writer = new FileWriter(outputFile);
		
		printFragmentBox(fragment,writer);
		
	}
	
	public static void printFragment(Box fragment, File outputDirectory, String encoding) throws IOException {
		if(!outputDirectory.isDirectory()){
			throw new IOException("Directory '"+ outputDirectory.toString() +"' is not a directory.");
		}
		String fileName = fragment.resourceName();
		File outputFile = new File(outputDirectory, fileName);
		if(!outputFile.getParentFile().exists()){
			outputFile.getParentFile().mkdirs();
		}
		OutputStream out = new FileOutputStream(outputFile);
		OutputStreamWriter writer = new OutputStreamWriter(out,Charset.forName(encoding));
		
		printFragmentBox(fragment,writer);
		
	}
	
	public static void printFragmentBox(Box box, Writer out) throws IOException{
		out.append(box.fragment().toString());
		out.flush();
		out.close();
	}
	
}
