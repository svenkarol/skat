/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.java;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import static org.skat.isc.core.IOHelper.*;

import org.skat.binding.java.FragmentParser.ParseException;
import org.skat.binding.java.ast.ASTNode;
import org.skat.binding.java.ast.Box;
import org.skat.binding.java.ast.BytecodeParser;
import org.skat.binding.java.ast.CompilationUnit;
import org.skat.binding.java.ast.CompositionProgram;
import org.skat.binding.java.ast.CompositionSystem;
import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.binding.java.ast.JavaParser;
import org.skat.binding.java.ast.List;
import org.skat.binding.java.ast.Options;
import org.skat.binding.java.ast.Program;

/**
 * This class holds the composition API for Java, based on the full ISC base and the JastAddJ
 * compiler. 
 * 
 * @author skarol
 *
 */
public class JavaCompositionSystem extends CompositionSystem {

	final private static String INTERNAL_PREFIX = "~jSkAT_ID";
	private int idCounter = 0; 
	private String encoding = null;
	
	public JavaCompositionSystem(String baseURI) throws IOException {
		super(baseURI);
	}
	
	public JavaCompositionSystem(File baseDirectory) throws IOException{
		this(baseDirectory.toURI().toString());
	}

	public JavaCompositionSystem(File baseDirectory, String inFolder, String outFolder) throws IOException{
		this(baseDirectory.toURI().toString(),inFolder,outFolder);
	}
	
	/**
	 * Creates a new instance of a Java compositon system. 
	 * 
	 * @param baseURI The base location URI, should be absolute.
	 * @param inFolder The input fragment folder, can be absolute or relative to the baseURI.
	 * @param outFolder The output fragment folder, can be absolute or relative to the baseURI. 
	 * @throws IOException
	 */
	public JavaCompositionSystem(String baseURI, String inFolder, String outFolder) throws IOException {
		super(baseURI,inFolder,outFolder);
	}
	
	@Override
	protected CompositionEnvironment createEnvironment() throws IOException {
		File fragmentDirectory = new File(URI.create(this.getInURI()));
		return createEnvironment(fragmentDirectory);
	}
	
	@Override
	public boolean isConsistent() {
		if(jastaddj_checkCompilationUnits(getEnv())){
			return false;
		}
		return true;
	}
	
	public String genID(){
		return INTERNAL_PREFIX + idCounter++;
	}
	
	/**
	 * Tries parsing a fragment string first without the fragment delimiter, if that fails, it ties to 
	 * use the fragment delimiter as a prefix and parses the prefixed fragment. If both attempts frail,
	 * it raises an exception.
	 * 
	 * @param content the fragment content as a string
	 * @param fragmentID the name of the fragment box to be created
	 * @return The fragment box containing the fragment AST representing the content string.
	 * @throws IOException
	 * @throws FragmentParser.ParseException
	 */
	private Box tryParse(String fragmentID, String content) throws IOException, FragmentParser.ParseException{
		Box fragment = (Box)FragmentParser.parseFragment(content, fragmentID, true);
		return fragment;
	}
	
	/**
	 * Add a new Bind operator to the contained environment by creating a new fragment box from the content
	 * string and adding a new Bind instance to the environment.
	 * 
	 * @param pointName - the name of the slot to be bound
	 * @param content - the content to be bound as string
	 * @return The generated fragment ID.
	 * @throws IOException
	 * @throws FragmentParser.ParseException
	 */
	public String addBindContent(String pointName, String content) throws IOException, FragmentParser.ParseException{
		String fragmentID = genID();
		Box fragment = tryParse(fragmentID,content);
		getEnv().getFragmentList().add(fragment);
		this.addBind(pointName,fragmentID);
		return fragmentID;
	}
	
	public String addBindTerminal(String pointName, Object content) throws IOException, FragmentParser.ParseException{
		String fragmentID = genID();		
		Box fragment = FragmentParser.createTerminalFragment(content,fragmentID);
		getEnv().getFragmentList().add(fragment);
		this.addBind(pointName,fragmentID);
		return fragmentID;
	}
	
	/**
	 * Parses a Java fragment from text and adds it to the environment.
	 * 
	 * @param fragmentName
	 * @param content
	 * @return The new fragment box.
	 * @throws ParseException
	 * @throws IOException
	 */
	public Box addFragment(String fragmentName, String content) throws ParseException, IOException{
		Box fragment = this.tryParse(fragmentName,content);
		this.getEnv().getFragmentList().addChild(fragment);
		return fragment;
	}

	/**
	 * Add a new Extend operator to the contained environment by creating a new fragment box from the content
	 * string and adding a new Bind instance to the environment.
	 * 
	 * @param pointName - the name of the hook to be extended
	 * @param content - the content to be bound as string
	 * @return The generated fragment ID.
	 * @throws IOException
	 * @throws FragmentParser.ParseException
	 */
	public String addExtendContent(String pointName, String content) throws IOException, FragmentParser.ParseException{
		String fragmentID = genID();
		Box fragment = tryParse(fragmentID,content);
		getEnv().getFragmentList().add(fragment);
		this.addExtend(pointName,fragmentID);
		return fragmentID;
	}
	
	/**
	 * Uses the FragmentPrinter to persist a fragment box.
	 * 
	 * 
	 */
	@Override
	public void doPersist(Box fragment) throws IOException{
		if(getEncoding()==null)
			FragmentPrinter.printFragment(fragment,new File(URI.create(getOutURI())));		
		else 
			FragmentPrinter.printFragment(fragment,new File(URI.create(getOutURI())),getEncoding());	
	}

	private CompositionEnvironment createEnvironment(File fragmentDirectory) throws IOException{
		CompositionEnvironment environment = new CompositionEnvironment();
		environment.state().reset();
		environment.initBytecodeReader(new BytecodeParser());
		environment.initPaths();
		environment.initJavaParser(
			new JavaParser() {
				public CompilationUnit parse(java.io.InputStream is, String fileName)
						throws java.io.IOException, beaver.Parser.Exception {
							return new org.skat.binding.java.parser.JavaParser().parse(is, fileName);
				}
		});
		jastaddj_initOptions(environment);
		//environment.options().addOptions(readFileURLs(new File[] {}));
		loadFragments(environment,fragmentDirectory);
		return environment;
	}



	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void loadFragments(CompositionEnvironment environment, File fragmentDirectory) throws IOException {	
		Collection<File> fragmentFiles = new LinkedList<File>();
		fragmentFiles.add(fragmentDirectory);
		fragmentFiles = readFiles(fragmentFiles, new String[] {".java",".jbx"});
		assert(fragmentFiles.size() > 0);
		for (File currentFile:fragmentFiles) {
				ASTNode fragmentContainer = FragmentParser.parseFragment(new FileInputStream(
						currentFile), currentFile.getName(), getEncoding());
				if(fragmentContainer instanceof Box)
					environment.getFragmentList().add((Box)fragmentContainer);
				else if(fragmentContainer.isList())
					environment.getFragmentList().addAll((List<Box>)fragmentContainer);
	
		}
		
		environment.setCompositionProgram(new CompositionProgram());
	}
	//methods to initialize jastaddj
	private static void jastaddj_initOptions(CompositionEnvironment environment) {
		Options options = environment.options();
		options.initOptions();
		options.addKeyOption("-version");
		options.addKeyOption("-print");
		options.addKeyOption("-g");
		options.addKeyOption("-g:none");
		options.addKeyOption("-g:lines,vars,source");
		options.addKeyOption("-nowarn");
		options.addKeyOption("-verbose");
		options.addKeyOption("-deprecation");
		options.addKeyValueOption("-classpath");
		options.addKeyValueOption("-cp");
		options.addKeyValueOption("-sourcepath");
		options.addKeyValueOption("-bootclasspath");
		options.addKeyValueOption("-extdirs");
		options.addKeyValueOption("-d");
		options.addKeyValueOption("-encoding");
		options.addKeyValueOption("-source");
		options.addKeyValueOption("-target");
		options.addKeyOption("-help");
		options.addKeyOption("-O");
		options.addKeyOption("-J-Xmx128M");
		options.addKeyOption("-recover");
	}
	

	@SuppressWarnings("rawtypes")
	public static boolean jastaddj_checkCompilationUnits(Program program) {
		try {
			for (CompilationUnit unit : program.getCompilationUnits()) {
				if (unit.fromSource()) {
					Collection errors = unit.parseErrors();
					Collection warnings = new LinkedList();
					// compute static semantic errors when there are no parse
					// errors or
					// the recover from parse errors option is specified
					if (errors.isEmpty()
							|| program.options().hasOption("-recover"))
						unit.errorCheck(errors, warnings);
					if (!errors.isEmpty()) {
						jastaddj_processErrors(errors, unit);
						return false;
					} else {
						if (!warnings.isEmpty())
							jastaddj_processWarnings(warnings, unit);
						jastaddj_processNoErrors(unit);
					}
				}
			}

		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
		return true;
	}

	@SuppressWarnings("rawtypes")
	private static void jastaddj_processErrors(Collection errors, CompilationUnit unit) {
		System.err.println("Errors:");
		for (Iterator iter2 = errors.iterator(); iter2.hasNext();) {
			System.err.println(iter2.next());
		}
	}

	private static void jastaddj_processNoErrors(CompilationUnit unit) {
	}

	@SuppressWarnings("rawtypes")
	private static void jastaddj_processWarnings(Collection warnings,
			CompilationUnit unit) {
		System.err.println("Warnings:");
		for (Iterator iter2 = warnings.iterator(); iter2.hasNext();) {
			System.err.println(iter2.next());
		}
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	
}
