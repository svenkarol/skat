// SLOTIDENTIFIER for enabling a unique slot syntax 
<YYINITIAL> {
    "[[" ([:jletter:]|[\ud800-\udfff])([:jletterdigit:]|[\ud800-\udfff])* "]]"  { return sym(Terminals.SLOTIDENTIFIER); }
}
