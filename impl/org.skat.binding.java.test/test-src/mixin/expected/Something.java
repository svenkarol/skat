public class Something implements ISubjectMixin {
  public void doSomething() {
    notifyObservers();
  }
  private java.util.List<Observer> observers = new java.util.LinkedList<Observer>();
  public void notifyObservers() {
    notifyObservers();
    for (Observer o : observers) {
      o.reportChange(this);
    }
  }
  public void addObserver(Observer o) {
    notifyObservers();
    observers.add(o);
  }
  public void removeObserver(Observer o) {
    notifyObservers();
    observers.remove(o);
  }
}