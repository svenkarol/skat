public class SubjectMixin implements ISubjectMixin {
  private java.util.List<Observer> observers = new java.util.LinkedList<Observer>();
  public void notifyObservers() {
    for (Observer o : observers) {
      o.reportChange(this);
    }
  }
  public void addObserver(Observer o) {
    observers.add(o);
  }
  public void removeObserver(Observer o) {
    observers.remove(o);
  }
}