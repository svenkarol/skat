import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Main2 {
	
	
	public static void main(String[] args) throws IOException, InterruptedException, ExecutionException {
/*		List<Pair<String,Integer>> testList = new LinkedList<Pair<String,Integer>>();
		testList.add(new Pair("D",3));
		testList.add(new Pair("AA",6));
		testList.add(new Pair("F",1));
		testList.add(new Pair("C",4));
		testList.add(new Pair("B",5));	
		testList.add(new Pair("E",2));
		testList.add(new Pair("D",3));
		testList.add(new Pair("Aa",6));
		testList.add(new Pair("F",1));
		testList.add(new Pair("C",4));
		testList.add(new Pair("B",5));	
		testList.add(new Pair("E",2));
		sort(testList);
		for(Pair<String,Integer> pair:testList){		
			System.out.println("["+pair.KEY+","+pair.VAL+"]");
		}		
		if(true)
			return;*/
		final Map<String,String> globalStorage = IOHelper.getDocuments();	
		final Iterator<String> keys = globalStorage.keySet().iterator();
		int workers = 2;
		ExecutorService executor = Executors.newFixedThreadPool(workers);
		List<Future<Map<String,Integer>>> results = new LinkedList<Future<Map<String,Integer>>>();
		
		//splitting and counting
		while(keys.hasNext()){
			final String text = globalStorage.get(keys.next());
			results.add(executor.submit(
					new Op<Map<String,Integer>>(){
						@Override
						public Map<String, Integer> call() throws Exception {
							return map(text);
						}}
					));
		}
		
		Map<String,Integer> integratedResult = new HashMap<String,Integer>();
		
		//merging and counting
		while(!results.isEmpty()){
			for(Future<Map<String,Integer>> future:results){
				if(future.isDone()){
					Map<String,Integer> intermediate = future.get();
					merge(intermediate,integratedResult);
					results.remove(future);
					break;
				}
			}
		}
		
		//initializing buckets for concurrent search, bucket search is only useful, 
		//if element distribution to buckets is much less expensive than a direct comparison,
		//e.g., alphabetical order.
		
		final char non_ascii_mask = 0xFF00;	
		final byte bucket_count = 16;
		final double potency = Math.log(bucket_count)/Math.log(2);
		if(Math.rint(potency)!=potency){
			executor.shutdown();
			throw new RuntimeException("Number of buckets must be a whole potency of 2.");
		}
		final byte r_shift_amount = (byte) (potency-1); 	 
		final byte bucket_mask = (byte) (~(0xFF>>r_shift_amount));
		
		List<Pair<String,Integer>>[] buckets = new List[bucket_count+1];
		
		for(String word:integratedResult.keySet()){
			//System.out.println(word+ "::::" + integratedResult.get(word));
			char first = word.charAt(0);
			int index = 0;
			if((first&non_ascii_mask)==0){
				index = ((bucket_mask&first)>>8-r_shift_amount)+1;
				//System.out.println(Integer.toBinaryString(first)+"---->"+index);			
			}
			if(buckets[index]==null){
				buckets[index] = new LinkedList<Pair<String,Integer>>(); 
			}
			buckets[index].add(new Pair<String,Integer>(word,integratedResult.get(word)));
		}
		

		
		for(int i=0;i<buckets.length;i++){
			final List<Pair<String,Integer>> bucket = buckets[i];
			if(bucket!=null){
				System.out.println("Bucket "+i+ " " + bucket.size());
				executor.submit(new Op<List<Pair<String,Integer>>>(){
					@Override
					public List<Pair<String, Integer>> call() throws Exception {
						sort(bucket);		
						return bucket;
					}
				});
			}
			else{
				System.out.println("Bucket null");
			}
		}
		
		//don't forget the shutdown to avoid Java zombies
		executor.shutdown();
		while(!executor.isTerminated()){
			//wait
		}
		
		for(int i=0;i<buckets.length;i++){
			if(buckets[i]==null)
				continue;
			for(Pair<String,Integer> pair:buckets[i]){		
				System.out.println("["+pair.KEY+","+pair.VAL+"]");
			}
		}
	
		
		System.out.println("done(2)");
	}
	
	public interface Op<V> extends Callable<V>, Cloneable {}
	
	
	public static boolean isWS(char c){
		if(c < '!' || c ==']' || c=='|' || c=='[' || c=='=' || c=='"' || c=='\'' || c=='<' || c=='>' || c=='{' || c=='}' || c=='(' || c==')')
			return true;
		return false;
	}
	

	
	public static void sort(List<Pair<String,Integer>> list){
		if(list==null||list.size()<=1){
			return;
		}
		sort(list,0,list.size()-1);
	}
	
	public static void sort(List<Pair<String,Integer>> list, int beginIdx, int endIdx){
		if(beginIdx<endIdx){
			int pivotIdx = (endIdx-beginIdx)/2+beginIdx;
			int storeIdx = partition(list, beginIdx, endIdx, pivotIdx);
			sort(list,beginIdx,storeIdx-1);
			sort(list,storeIdx+1,endIdx);
		}
	}
	
	public static int partition(List<Pair<String,Integer>> list, int beginIdx, int endIdx, int pivotIdx){
		String pivotString = list.get(pivotIdx).KEY;
		list.get(pivotIdx).swap(list.get(endIdx));
		
		int storeIdx = beginIdx;
		for(int i=beginIdx;i<endIdx;i++){
			String keyVal = list.get(i).KEY;
			if(keyVal.compareTo(pivotString)<0){
				list.get(i).swap(list.get(storeIdx));
				storeIdx++;
			}
		}
		list.get(storeIdx).swap(list.get(endIdx));
		return storeIdx;
	}
	
	public static Map<String,Integer> map(String text){
		Map<String,Integer> storage = new HashMap<String,Integer>();
		int textSize = text.length();
		for(int i=0;i<textSize;){
			while(i<textSize&&isWS(text.charAt(i)))
				i++;
			int start = i;
			while(i<textSize&&!isWS(text.charAt(i)))
				i++;
			if(start<i){
				String word = text.substring(start,i);
				if(storage.containsKey(word))
					storage.put(word,storage.get(word)+1);
				else
					storage.put(word,1);
			}									
		}
		return storage;
	}
	
	public static void merge(Map<String,Integer> intermediate, Map<String,Integer> integrationTarget){
		for(String string:intermediate.keySet()){
			if(integrationTarget.containsKey(string)){
				integrationTarget.put(string,integrationTarget.get(string)+intermediate.get(string));
			}
			else{
				integrationTarget.put(string,intermediate.get(string));
			}
		}
	}
	
}
