import java.util.HashMap;
import java.util.Map;



abstract class MapRunner<V1,V2,V3,V4> extends OpRunner<V1,V2,Map<V3,V4>> implements Runnable, Cloneable {
	
	
	protected Map<V3,V4> bottomVal() {
		return new HashMap<V3,V4>();
	}
}