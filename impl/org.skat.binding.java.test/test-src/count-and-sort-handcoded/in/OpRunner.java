
public abstract class OpRunner<V1,V2,RT> implements Runnable, Cloneable {
	
	private RT val;
	
	@Override
	public final void run() {
		val = bottomVal();
		V1 next = null;
		synchronized(this){
			next = getNextKey();
		}
		while(next!=null){
			val = perform(next,getValue(next),val);
			synchronized(this){
				next = getNextKey();
			}
		}	
	}
	
	protected abstract V1 getNextKey();
	
	protected abstract V2 getValue(V1 key);
	
	protected abstract RT perform(V1 key,V2 value,RT curVal);
    
	protected abstract RT bottomVal();
	
	public RT getResult(){
		return val;
	}
	
	@SuppressWarnings("unchecked")
	public OpRunner<V1,V2,RT> clone() {
		try {
			return (OpRunner<V1, V2,RT>) super.clone();
		} catch (CloneNotSupportedException e) {				
			e.printStackTrace();
		}
		return null;
	}
	
}
