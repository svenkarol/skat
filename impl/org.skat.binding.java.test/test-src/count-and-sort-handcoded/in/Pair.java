class Pair<K,V>{
	
	public Pair(K key, V val){
		this.KEY = key;
		this.VAL = val;
	}
	
	public K KEY;
	public V VAL;
	
	public void swap(Pair<K,V> other){
		K oldKEY = KEY;
		V oldVAL = VAL;
		KEY = other.KEY;
		VAL = other.VAL;
		other.KEY = oldKEY;
		other.VAL = oldVAL;
	}
}