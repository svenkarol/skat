import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.LinkedList;
import java.util.ArrayList;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class TestMain {

	final public static URI localbase = (new File(".")).toURI();
	final public static int workers =10;
	 
	/**
	 * @param args
	 * @throws IOException 
	 * @throws IllegalAccessException 
	 * @throws InstantiationException 
	 * @throws ClassNotFoundException 
	 * @throws ClassCastException 
	 */
	public static void main(String[] args) throws IOException {
		final Map<String,String> globalStorage = IOHelper.getDocuments();	
		final Iterator<String> keys = globalStorage.keySet().iterator();
		
		MapRunner<String,String,String,Integer> worker1 = new MapRunner<String,String,String,Integer>(){
			@Override
			protected String getNextKey() {
				if(keys.hasNext())
					return keys.next();
				return null;
			}
			@Override
			protected String getValue(String key) {
				return globalStorage.get(key);
			}

	     @Override
			protected Map<String, Integer> perform(String key, String value, Map<String, Integer> curVal) {
				map(value,curVal);
				return curVal; 
			}
		
	
		};
		
		Runnable worker2 = worker1.clone();
		
		ExecutorService executor = Executors.newFixedThreadPool(workers);
		executor.execute(worker1);
		executor.execute(worker2);
		executor.shutdown();
		while(!executor.isTerminated());
		System.out.println("done");
	}
	
	public static void map(String text, Map<String,Integer> storage){
		int textSize = text.length();
		for(int i=0;i<textSize;){
			while(i<textSize&&isWS(text.charAt(i)))
				i++;
			int start = i;
			while(i<textSize&&!isWS(text.charAt(i)))
				i++;
			if(start<i){
				String word = text.substring(start,i);
				if(storage.containsKey(word))
					storage.put(word,storage.get(word)+1);
				else
					storage.put(word,1);
			}
				
		}
	}
	
	public static boolean isWS(char c){
		if(c < '!')
			return true;
		return false;
	}
	
}
