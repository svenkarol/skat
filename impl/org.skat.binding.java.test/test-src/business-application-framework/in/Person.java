public abstract class Person extends BusinessObject {
	private String firstName;
	private String lastName;
	private int age;
	
	public String getFirstName() {return firstName;}
	public void setFirstName(String firstName) 
			{this.firstName = firstName;}

	public String getLastName() {return lastName;}
	public void setLastName(String lastName) 
			{this.lastName = lastName;}
			
	public String getName() {return lastName + ", " + firstName;}

	public int getAge() {return age;}
	public void setAge(int age) {this.age = age;}
}