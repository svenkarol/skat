public abstract class BusinessObject {	
	private String id;
	
	public String getID(){return id;}
	public void setID(String id){this.setID(id);}
	
	public abstract String asString();	
}	