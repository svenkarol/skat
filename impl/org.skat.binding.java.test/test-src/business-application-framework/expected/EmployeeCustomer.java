public class EmployeeCustomer extends Person implements IEmployee, ICustomer {
  public EmployeeCustomer() {
    super();
    discount = 20;
  }
  public String asString() {
    String v = "EmployeeCustomer";
    v += "\n id:" + getID();
    v += "\n name:" + getName();
    v += "\n employed:" + getEmployed();
    v += "\n workload:" + getWorkload();
    v += "\n discount:" + getDiscount();
    return v;
  }
  private java.util.Date employed;
  private int workload;
  public void setEmployed(java.util.Date employed) {
    this.employed = employed;
  }
  public java.util.Date getEmployed() {
    return employed;
  }
  public void setWorkload(int workload) {
    this.workload = workload;
  }
  public int getWorkload() {
    return workload;
  }
  private int discount;
  public void setDiscount(int discount) {
    this.discount = discount;
  }
  public int getDiscount() {
    return discount;
  }
}