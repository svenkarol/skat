public class Shareholder extends Person {
  public Shareholder() {
    super();
  }
  public String asString() {
    String v = "Shareholder";
    v += "\n id:" + getID();
    v += "\n name:" + getName();
    v += "\n shares:" + getShares();
    return v;
  }
  public void setShares(int shares) {
    this.shares = shares;
  }
  public int getShares() {
    return shares;
  }
  private int shares;
}