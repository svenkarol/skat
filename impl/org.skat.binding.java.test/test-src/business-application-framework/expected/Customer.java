public class Customer extends Person implements ICustomer {
  public Customer() {
    super();
  }
  public String asString() {
    String v = "Customer";
    v += "\n id:" + getID();
    v += "\n name:" + getName();
    v += "\n discount:" + getDiscount();
    return v;
  }
  public void setDiscount(int discount) {
    this.discount = discount;
  }
  public int getDiscount() {
    return discount;
  }
  private int discount;
}