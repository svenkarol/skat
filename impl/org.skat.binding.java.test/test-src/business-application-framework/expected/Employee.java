public class Employee extends Person implements IEmployee {
  public Employee() {
    super();
  }
  public String asString() {
    String v = "Employee";
    v += "\n id:" + getID();
    v += "\n name:" + getName();
    v += "\n employed:" + getEmployed();
    v += "\n workload:" + getWorkload();
    return v;
  }
  public void setEmployed(java.util.Date employed) {
    this.employed = employed;
  }
  public java.util.Date getEmployed() {
    return employed;
  }
  private java.util.Date employed;
  public void setWorkload(int workload) {
    this.workload = workload;
  }
  public int getWorkload() {
    return workload;
  }
  private int workload;
}