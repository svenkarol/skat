public interface ICustomer {
  public String asString();
  public void setDiscount(int discount);
  public int getDiscount();
}