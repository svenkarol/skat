public interface IEmployee {
  public String asString();
  public void setEmployed(java.util.Date employed);
  public java.util.Date getEmployed();
  public void setWorkload(int workload);
  public int getWorkload();
}