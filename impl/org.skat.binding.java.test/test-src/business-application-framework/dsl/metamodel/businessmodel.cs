SYNTAXDEF bm
FOR <http://www.emftext.org/language/businessmodel>
START BusinessModel

RULES {
	BusinessModel ::= "application" "roles" "{" roleDefinitions* "}"; 
	RoleDefinition ::= "object" name[] ("is_a" superRoles[] ("," superRoles[])* )? (":" properties+)?; 
	PropertyDefinition ::= name[] ":" type; 
	Date ::=  "Date" ("[" default[] "]")?;
	Hours ::=   "Hours" ("[" default[] "]")?;   
	Percentage ::=  "Percentage" ("[" default[] "]")?;	
}