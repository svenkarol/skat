public class Item {
  private double price;
  private long itemNo;
  private String name;
  public Item(String name, long itemNo, double price) {
    super();
    this.price = price;
    this.itemNo = itemNo;
    this.name = name;
  }
  public double getPrice() {
    return price;
  }
  public long getItemNo() {
    return itemNo;
  }
  public String getName() {
    return name;
  }
  public double decreasePrice() {
    Logger.log("Changing price in " + "decreasePrice");
    if(price > 0) {
      this.price = price * 0.1D;
    }
    return price;
  }
  public void setItemNo(long itemNo) {
    this.itemNo = itemNo;
  }
  public void setPrice(double price) {
    Logger.log("Changing price in " + "setPrice");
    this.price = price;
  }
  public void setName(String name) {
    this.name = name;
  }
  private int field;
}