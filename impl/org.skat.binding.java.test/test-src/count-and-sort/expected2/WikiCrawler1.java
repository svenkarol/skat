import java.util.Collection;
import java.util.Iterator;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;

public class WikiCrawler1 {
  public Map<String, Integer> compute(Collection<String> in) throws Exception {
    Iterator<String> it = in.iterator();
    Collection<Map<String, Integer>> mapResult = null;
    {
      List<Map<String, Integer>> results = new LinkedList<Map<String, Integer>>();
      String inValue = it.hasNext() ? it.next() : null;
      while(inValue != null){
        Map<String, Integer> result = map(inValue);
        results.add(result);
        inValue = it.hasNext() ? it.next() : null;
      }
      mapResult = results;
    }
    Iterator<Map<String, Integer>> mIt = mapResult.iterator();
    Map<String, Integer> reduceResult = null;
    {
      Map<String, Integer> result = new HashMap<String, Integer>();
      Map<String, Integer> inValue = mIt.hasNext() ? mIt.next() : null;
      while(inValue != null){
        reduce(inValue, result);
        inValue = mIt.hasNext() ? mIt.next() : null;
      }
      reduceResult = result;
    }
    return reduceResult;
  }
  public static Map<String, Integer> map(String text) {
    Map<String, Integer> storage = new HashMap<String, Integer>();
    int textSize = text.length();
    for(int i = 0; i < textSize; ) {
      while(i < textSize && isWS(text.charAt(i)))
        i++;
      int start = i;
      while(i < textSize && !isWS(text.charAt(i)))
        i++;
      if(start < i) {
        String word = text.substring(start, i);
        if(storage.containsKey(word)) 
          storage.put(word, storage.get(word) + 1);
        else 
          storage.put(word, 1);
      }
    }
    return storage;
  }
  public static boolean isWS(char c) {
    if(c < '!' || c == ']' || c == '|' || c == '[' || c == '=' || c == '\"' || c == '\'' || c == '<' || c == '>' || c == '{' || c == '}' || c == '(' || c == ')') 
      return true;
    return false;
  }
  public static void reduce(Map<String, Integer> in1, Map<String, Integer> inout2) {
    for (String string : in1.keySet()) {
      if(inout2.containsKey(string)) {
        inout2.put(string, inout2.get(string) + in1.get(string));
      }
      else {
        inout2.put(string, in1.get(string));
      }
    }
  }
}