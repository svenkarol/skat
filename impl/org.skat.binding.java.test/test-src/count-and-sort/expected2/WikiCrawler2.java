import java.util.Collection;
import java.util.Iterator;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;

public class WikiCrawler2 {
  public Map<String, Integer> compute(Collection<String> in) throws Exception {
    Iterator<String> it = in.iterator();
    Collection<Map<String, Integer>> mapResult = null;
    {
      ExecutorService executor = Executors.newFixedThreadPool((2));
      List<Future<Map<String, Integer>>> futures = new LinkedList<Future<Map<String, Integer>>>();
      Map<String, Integer> reduceResult = new HashMap<String, Integer>();
      String inValue = it.hasNext() ? it.next() : null;
      while(inValue != null){
        final String inArg = inValue;
        futures.add(executor.submit(new Callable<Map<String, Integer>>() {
            public Map<String, Integer> call() throws Exception {
              Map<String, Integer> mapResult = map(inArg);
              return mapResult;
            }
        }));
        inValue = it.hasNext() ? it.next() : null;
      }
      while(futures.size() > 1){
        Future<Map<String, Integer>> current1 = futures.remove(0);
        if(current1.isDone()) {
          final Map<String, Integer> arg1 = current1.get();
          while(futures.size() > 0){
            Future<Map<String, Integer>> current2 = futures.remove(0);
            if(current2.isDone()) {
              final Map<String, Integer> arg2 = current2.get();
              futures.add(executor.submit(new Callable<Map<String, Integer>>() {
                  public Map<String, Integer> call() throws Exception {
                    reduce(arg2, arg1);
                    return arg1;
                  }
              }));
              break ;
            }
            else {
              futures.add(current2);
            }
          }
        }
        else {
          futures.add(current1);
        }
      }
      executor.shutdown();
      Future<Map<String, Integer>> lastResult = futures.get(0);
      while(!lastResult.isDone()){
      }
      reduceResult = lastResult.get();
      mapResult = java.util.Collections.singletonList(reduceResult);
    }
    Iterator<Map<String, Integer>> mIt = mapResult.iterator();
    Map<String, Integer> reduceResult = null;
    reduceResult = mIt.next();
    return reduceResult;
  }
  public static Map<String, Integer> map(String text) {
    Map<String, Integer> storage = new HashMap<String, Integer>();
    int textSize = text.length();
    for(int i = 0; i < textSize; ) {
      while(i < textSize && isWS(text.charAt(i)))
        i++;
      int start = i;
      while(i < textSize && !isWS(text.charAt(i)))
        i++;
      if(start < i) {
        String word = text.substring(start, i);
        if(storage.containsKey(word)) 
          storage.put(word, storage.get(word) + 1);
        else 
          storage.put(word, 1);
      }
    }
    return storage;
  }
  public static boolean isWS(char c) {
    if(c < '!' || c == ']' || c == '|' || c == '[' || c == '=' || c == '\"' || c == '\'' || c == '<' || c == '>' || c == '{' || c == '}' || c == '(' || c == ')') 
      return true;
    return false;
  }
  public static void reduce(Map<String, Integer> in1, Map<String, Integer> inout2) {
    for (String string : in1.keySet()) {
      if(inout2.containsKey(string)) {
        inout2.put(string, inout2.get(string) + in1.get(string));
      }
      else {
        inout2.put(string, in1.get(string));
      }
    }
  }
}