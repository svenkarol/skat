import java.util.Collection;
import java.util.Map;

public class App {
  public static void main(String[] args) throws Exception {
    Collection<String> in = Query.getWikiPages(args[0], Integer.parseInt(args[1])).values();
    Map<String, Integer> result = null;
    int cores = Runtime.getRuntime().availableProcessors();
    int threshold = args.length >= 4 ? Integer.parseInt(args[3]) : 10000;
    if(cores == 1 || threshold >= in.size()) {
      WikiCrawler1 impl = new WikiCrawler1();
      result = impl.compute(in);
    }
    if(cores >= 2 && threshold < in.size()) {
      WikiCrawler2 impl = new WikiCrawler2();
      result = impl.compute(in);
    }
    java.net.Socket sock = new java.net.Socket(args[2], 12345);
    (new java.io.ObjectOutputStream(sock.getOutputStream())).writeObject(result);
    sock.close();
  }
}