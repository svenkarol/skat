import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.net.URLConnection;
import java.util.Map;
import java.util.HashMap;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSParser;


public class Query {
	
	final public static String SEARCH_PREFIX = "A";
	final public static int GAP_LIMIT = 100;

	
	public static Map<String,String> getWikiPages(String prefix, int gapLimit) throws IOException{
		BufferedReader in = null;
		try{
			URL url = new URL("http://en.wikipedia.org/w/api.php?format=xml" +
                    				"&action=query" +
                    				"&prop=revisions" +
									"&rvprop=content" +
                    				"&generator=allpages" +
									"&gaplimit=" + gapLimit +
									"&gapfilterredir=nonredirects"+
									"&gapfrom="+prefix);

	        URLConnection connection = url.openConnection();
	        connection.setRequestProperty("User-Agent","TestBot/0.1");
	        connection.setRequestProperty("Accept-Charset","UTF-8");
	        in = new BufferedReader(
	                                new InputStreamReader(
	                                connection.getInputStream(),"UTF-8"));
	        Map<String,String> docs = initializeDocuments(in);
	        return docs;
		}
		finally{
			if(in!=null)
	        in.close();
		}

	}
	
	
	
	public static Document parseXML(Reader in){
    	DOMImplementationRegistry registry = null;
	    try {
			registry = DOMImplementationRegistry.newInstance();
		} catch (ClassCastException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	    DOMImplementationLS impl = (DOMImplementationLS)registry.getDOMImplementation("LS 3.0");
	    LSParser parser = impl.createLSParser(DOMImplementationLS.MODE_SYNCHRONOUS,
				"http://www.w3.org/2001/XMLSchema");
	    LSInput input = impl.createLSInput();
	    input.setCharacterStream(in);
	    return parser.parse(input);
	}
	
    public static Map<String,String> initializeDocuments(Reader in) {
        Document document = parseXML(in);
        Map<String,String> docs = new HashMap<String,String>(GAP_LIMIT,0.75f); 
        NodeList pages = document.getElementsByTagName("page");
        for(int i=0;i<pages.getLength();i++){
        	Node pageNode = pages.item(i);
        	NodeList revisions = pageNode.getChildNodes();
        	for(int j=0;j<revisions.getLength();j++){
        		Node revisionsNode = revisions.item(j);
        		if("revisions".equals(revisionsNode.getNodeName())){
        			NodeList revs = revisionsNode.getChildNodes();
        			for(int k=0;k<revs.getLength();k++){
        				Node revNode = revs.item(k);
        				if("rev".equals(revNode.getNodeName())){
        					docs.put(pageNode.getAttributes().getNamedItem("title").getNodeValue(),revNode.getTextContent());
        					break;
        				}
        			}
        		}
        	}
        }
        return docs;
    }

}
