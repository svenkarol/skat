import java.util.Collection;
import java.util.Iterator;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.LinkedList;
import java.util.List;
import java.util.HashMap;

public class WikiCrawlerA {
  public Collection<Map<String, Integer>> compute(Collection<String> in) throws Exception {
    Iterator<String> it = in.iterator();
    Collection<Map<String, Integer>> mapResult = null;
    {
      ExecutorService executor = Executors.newFixedThreadPool((2));
      List<Future<Map<String, Integer>>> futures = new LinkedList<Future<Map<String, Integer>>>();
      final List<Map<String, Integer>> results = new LinkedList<Map<String, Integer>>();
      String inValue = it.hasNext() ? it.next() : null;
      while(inValue != null){
        final String inArg = inValue;
        futures.add(executor.submit(new Callable<Map<String, Integer>>() {
            public Map<String, Integer> call() throws Exception {
              Map<String, Integer> result = map(inArg);
              synchronized(results) {
                results.add(result);
              }
              return result;
            }
        }));
        inValue = it.hasNext() ? it.next() : null;
      }
      executor.shutdown();
      while(!executor.isTerminated()){
      }
      mapResult = results;
    }
    return mapResult;
  }
  public static Map<String, Integer> map(String text) {
    Map<String, Integer> storage = new HashMap<String, Integer>();
    int textSize = text.length();
    for(int i = 0; i < textSize; ) {
      while(i < textSize && isWS(text.charAt(i)))
        i++;
      int start = i;
      while(i < textSize && !isWS(text.charAt(i)))
        i++;
      if(start < i) {
        String word = text.substring(start, i);
        if(storage.containsKey(word)) 
          storage.put(word, storage.get(word) + 1);
        else 
          storage.put(word, 1);
      }
    }
    return storage;
  }
  public static boolean isWS(char c) {
    if(c < '!' || c == ']' || c == '|' || c == '[' || c == '=' || c == '\"' || c == '\'' || c == '<' || c == '>' || c == '{' || c == '}' || c == '(' || c == ')') 
      return true;
    return false;
  }
}