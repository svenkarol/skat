import java.util.Collection;

public class MainA {
  public static void main(String[] args) throws Exception {
    WikiCrawlerA counter = new WikiCrawlerA();
    Collection<String> documents = Query.getWikiPages("A", 100).values();
    counter.compute(documents);
  }
}