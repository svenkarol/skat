import java.util.Collection;

public class MainD {
  public static void main(String[] args) throws Exception {
    WikiCrawlerD counter = new WikiCrawlerD();
    Collection<String> documents = Query.getWikiPages("A", 100).values();
    counter.compute(documents);
  }
}