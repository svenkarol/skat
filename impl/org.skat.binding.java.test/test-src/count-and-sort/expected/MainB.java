import java.util.Collection;

public class MainB {
  public static void main(String[] args) throws Exception {
    WikiCrawlerB counter = new WikiCrawlerB();
    Collection<String> documents = Query.getWikiPages("A", 100).values();
    counter.compute(documents);
  }
}