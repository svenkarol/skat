import java.util.Collection;

public class MainC {
  public static void main(String[] args) throws Exception {
    WikiCrawlerC counter = new WikiCrawlerC();
    Collection<String> documents = Query.getWikiPages("A", 100).values();
    counter.compute(documents);
  }
}