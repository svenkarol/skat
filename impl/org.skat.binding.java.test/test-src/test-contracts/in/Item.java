
public class Item extends SuperItem{
 
	private double price;
	private long itemNo;
	private String name;
	
	public Item(String name, long itemNo, double price){
		this.price = price;
		this.itemNo = itemNo;
		this.name = name;
	}

	
	public double getPrice() {
		return price;	
	}

	public long getItemNo() {
		return itemNo;
	}

	public String getName() {
		return name;
	}
	
	public void initPrice(){
		price = [[PriceSlot]];
		[[OtherSlot]] = 3*4;
	}

}
