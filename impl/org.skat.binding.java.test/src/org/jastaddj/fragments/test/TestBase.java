/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import static org.junit.Assert.assertNotNull;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.Iterator;

import org.skat.binding.java.FragmentParser.ParseException;
import org.skat.binding.java.JavaCompositionSystem;
import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.isc.core.StatusDescriptor;

import static org.skat.isc.core.IOHelper.*;

public abstract class TestBase {
	
	protected JavaCompositionSystem cSys = null;
	protected File baseDirectory = new File("test-src/");
	protected File expected = null;

	protected void doSetUp(String inDir, String outDir, String expectedDir) throws IOException{
		try{
			cSys = new JavaCompositionSystem(baseDirectory.toURI()
					.toString(),inDir,outDir);
			cSys.setEncoding("UTF-8");
			clearOut();
			expected = new File((URI.create(cSys.getBaseURI()).resolve(expectedDir)));
		
		}
		catch (ParseException e){
			System.out.println(e.getDetailedMessage());
			Assert.fail("Parse error during setup: " +e.getMessage()+ ".");
		}
		
		System.out.println("*********************** Executing test-example " + getClass().getSimpleName() + " ***********************");
		System.out.println("Provided fragment directory '" + inDir + "' ");
		System.out.println("In location  : " + cSys.getInURI());
		System.out.println("Base location: " + cSys.getBaseURI());
		System.out.println("Out location : " + cSys.getOutURI());
		System.out.println("Expected location  : " + expected.toURI().toString());
	
		CompositionEnvironment env = cSys.getEnv();
		assertNotNull(env);
		System.out.println("Fragments loaded from fragment directory: <" + env.getNumFragment()+">");
	}
	
	protected void clearOut(){
		File out = new File(URI.create(cSys.getOutURI()));
		if(out.exists()){
			File[] subFiles = out.listFiles();
			for(int i=0;i<subFiles.length;i++){
				if(!subFiles[i].getName().startsWith("."))
					subFiles[i].delete();
			}
			
		}
	}
	
	/**
	 * Compare the content of expected files and actual output of the composition using the encoding currently set
	 * in the composition system. Before the composition, newline encodings of Windows and Mac are normalized to Unix. 
	 */
	protected void compareExpected(){
		System.out.print("Comparing contents of out folder with expected files ... ");
		File out = new File(URI.create(cSys.getOutURI()));
		Assert.assertTrue(out.exists());
		Assert.assertTrue(expected.exists());
		File[] outList = out.listFiles();
		File[] expectedList = expected.listFiles();
		Assert.assertEquals("Number of expected files differs.",expectedList.length,outList.length);
		for(int i=0;i<expectedList.length;i++){
			if(!expectedList[i].getName().startsWith(".")){
				Assert.assertEquals(expectedList[i].getName(),outList[i].getName());
				try {
					String expected = readFile(expectedList[i],cSys.getEncoding())
							.replaceAll("\\r\\n", "\n").
								replaceAll("\\r", "\n");
					String actualOut = readFile(outList[i],cSys.getEncoding())
							.replaceAll("\\r\\n", "\n").
							     replaceAll("\\r", "\n");;
					Assert.assertEquals(expected,actualOut);
				} catch (IOException e) {
					Assert.fail(e.getMessage());
				}
			}
		}
		System.out.println("done.");
	}
	
}
