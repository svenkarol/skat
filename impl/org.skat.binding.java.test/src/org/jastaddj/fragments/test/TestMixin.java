/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.skat.binding.java.JavaCompositionSystem;
import org.skat.binding.java.ast.BodyDecl;
import org.skat.binding.java.ast.Box;
import org.skat.binding.java.ast.CompilationUnit;
import org.skat.binding.java.ast.CompositionSystem;
import org.skat.binding.java.ast.FieldDecl;
import org.skat.binding.java.ast.FieldDeclaration;
import org.skat.binding.java.ast.JavaFragmentBox;
import org.skat.binding.java.ast.CompilationUnitBox;
import org.skat.binding.java.ast.ClassBox;
import org.skat.binding.java.ast.MemberBox;
import org.skat.binding.java.ast.MemberDecl;
import org.skat.binding.java.ast.MethodDecl;
import org.skat.binding.java.ast.MethodBox;
import org.skat.binding.java.ast.Opt;


/**
 * A simple mixin-based inheritance example that shows how ISC can be used for multiple inheritance in Java
 * and what composition conflicts may occur.
 */
public class TestMixin extends TestBase {
	
	@Before
	public void setUp() throws Exception {
		doSetUp("mixin/in","mixin/out","mixin/expected");
	}
	
	@Test
	public void testMixin() throws IOException{
		//- composition conflict: adding notification to all added methods, also to the called one!
		//contract could be: do not add notification to called methods themselves if parameters are not
		//manipulated
		//- alternatively split the composition or add negation to path
		//- or strategy:
		cSys.setCompositionStrategy(CompositionSystem.POINT_ORDERED_COMPOSITION);
		//cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION);
		cSys.addExtendContent("Something.java#*.methodEntry", "notifyObservers();");
		mixin("Something.java","SubjectMixin.java",(JavaCompositionSystem)cSys);
		cSys.triggerComposition();
		
		CompilationUnit cu = (CompilationUnit)((JavaFragmentBox)cSys.getEnv().findFragment("Something.java")).getFragment();
		cu.flushCache();
		Collection<Object> coll = new LinkedList<Object>();
		System.out.print("Checking semantics (may take some time): ");
		cu.errorCheck(coll);
		if(!coll.isEmpty()){
			for(Object o:coll){
				System.out.println(o);
			}
			System.out.println(cu.dumpTree());
		}
		else {System.out.println("OK.");}
		Assert.assertTrue(coll.isEmpty());
		
		cSys.persistFragments();
		compareExpected();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void mixin(String subject, String mixin, JavaCompositionSystem cSys) throws IOException{
		CompilationUnitBox box = (CompilationUnitBox) cSys.findFragment(mixin);
		if(box==null)
			return;
	
		// First an interface is derived from the mixin class.
		ClassBox mixinBox = box.extractClassBox();
		String mixinName = mixinBox.getFragment().getID();
		String interfaceName = "I"+ mixinName;
		if(cSys.findFragment(interfaceName)==null)
			cSys.addFragment(interfaceName, "public interface " + interfaceName + " {}");
		
		// The 'implements' hook of the subject is extended with the new name.
		cSys.addExtendContent(subject + "#*.implements", interfaceName);
		cSys.addExtendContent(mixin + "#*.implements", interfaceName);
		
		// Generate extend declarations:
		for(BodyDecl decl : mixinBox.getFragment().getBodyDeclsNoTransform()){
	
			// Extend subject class and interface 'members' hook with method declarations.			
			if(decl instanceof MethodDecl){
				MethodDecl m = (MethodDecl)decl;
				MethodDecl m_interface = new MethodDecl(m.getModifiers(),m.getTypeAccess(),m.getID(), m.getParameterList(),m.getExceptionList(), new Opt());
				String id = cSys.genID();
				String i_id = cSys.genID();
				cSys.addFragment(new MethodBox(id,id,m));
				cSys.addFragment(new MethodBox(i_id,i_id,m_interface));
				cSys.addExtend(subject+"#*.members",id);
				cSys.addExtend(interfaceName+"#*.members", i_id);
			}
			// Extend subject class with field declarations of the mixin.
			else if(decl instanceof FieldDecl||decl instanceof FieldDeclaration){
				MemberDecl f = (MemberDecl)decl;
				String id = cSys.genID();
				cSys.addFragment(new MemberBox(id,id,f));
				cSys.addExtend(subject+"#*.members",id);
			}
		}
		
		// Configuration & Transformation phase:
		cSys.triggerComposition();
		if(!cSys.isEnvHealthy())
			cSys.printStatus();
		cSys.clearCompositionProgram();
	}

}
