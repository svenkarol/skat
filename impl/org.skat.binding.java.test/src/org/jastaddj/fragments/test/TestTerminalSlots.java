/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.skat.binding.java.ast.Composer;

public class TestTerminalSlots extends TestBase {
	@Before
	public void setUp() throws IOException {
		doSetUp("terminal-slots/in","terminal-slots/out","terminal-slots/expected");
	}

	@Test
	public void test() throws Exception {
		cSys.addBindContent("myMethodName", "theActualName");
		cSys.addBindContent("myConsName", "Cons");
		cSys.addBindTerminal("StrSlot", "Test");
		
		Assert.assertNotNull(cSys.getEnv().findSlot("myConsName"));
		Assert.assertNotNull(cSys.getEnv().findSlot("myConsName").composer().srcFragment());
		Assert.assertNotNull(cSys.getEnv().findSlot("myConsName").composer());
		Assert.assertNotNull(cSys.getEnv().findSlot("StrSlot"));
		
		cSys.triggerComposition();
		cSys.persistFragments("method.jbx");
		cSys.persistFragments("constructor.jbx");
		this.compareExpected();		
	}
}
