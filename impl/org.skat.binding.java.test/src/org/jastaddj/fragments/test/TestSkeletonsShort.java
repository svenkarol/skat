/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.skat.binding.java.FragmentParser.ParseException;
import org.skat.binding.java.ast.CompositionSystem;

public class TestSkeletonsShort extends TestBase{
	
	
	@Before
	public void setUp() throws IOException{
		doSetUp("count-and-sort/in","count-and-sort/out","count-and-sort/expected2");
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION_FP);			
		cSys.setRecoverMode(true);
	}
	
	@Test
	public void compositionProgram() throws IOException{
		// Parameterize and extend variant templates.
		composeVariant("WikiCrawler1",1);
		composeVariant("WikiCrawler2",2);
		
		// Compose main application class and dispatch code.
		cSys.copyBox("App.jbx","App.java");
		cSys.addBindContent("App.java#REQUEST_STMT", 
			"Query.getWikiPages(args[0],Integer.parseInt(args[1])).values()");
		String mainHook = "App.java#App.main.methodExit";
		cSys.addExtendContent(mainHook, 
			"STMT::= int cores = Runtime.getRuntime().availableProcessors();");
		cSys.addExtendContent(mainHook, 
				"STMT::= int threshold = args.length>=4?Integer.parseInt(args[3]):10000;");

		weaveDispatch("WikiCrawler1",1);
		weaveDispatch("WikiCrawler2",2);

		cSys.addExtendContent(mainHook, 
			"STMT::= java.net.Socket sock = new java.net.Socket(args[2], 12345);");
		cSys.addExtendContent(mainHook, 
		"(new java.io.ObjectOutputStream(sock.getOutputStream())).writeObject(result);"); 
		cSys.addExtendContent(mainHook, "sock.close();");
		
		// Execute composition and write to output directory.
		cSys.triggerComposition();
		cSys.persistFragments(".*\\.java");
		cSys.printStatus();
		
		compareExpected();
	}
	
	private void composeVariant(String variantName, int workers) throws IOException {
		// Create copy of variant template.
		cSys.copyBox("QueryCountMapReduce.jbx", variantName + ".java");
		
		// Weave import statements.
		weaveImportComposers(variantName + ".java");
		
		// Parameterize map slot depending on worker count.
		if(workers > 1){
			cSys.addBind(variantName + ".java#MAP_SKEL", "concurrent_mapreduce.jbx");			
			cSys.addBindContent(variantName + ".java#WORKERS", "(" + workers + ")");
		}
		else {
			cSys.addBind(variantName + ".java#MAP_SKEL", "simple_map.jbx");			
		}
		
		// Bind and extend variant with map-dependent stuff.
		String mapPort = "it.hasNext()?it.next():null";
		String mapType = "Map<String, Integer>";
		cSys.addBindContent(variantName + ".java#VAR_NAME", variantName);
		cSys.addExtend(variantName + ".members", "map_op.jbx_0");
		cSys.addExtend(variantName + ".members", "map_op.jbx_1");
		cSys.addBindContent(variantName + ".java#IN_PORT_INIT", mapPort);		
		cSys.addBindContent(variantName + ".java#IN_PORT_CONT", mapPort);		
		cSys.addBindContent(variantName + ".java#IN_TYPE", "String");
		cSys.addBindTerminal(variantName + ".java#MAP_OP", "map");
		cSys.addBindContent(variantName + ".java#OUT_TYPE", mapType);
		cSys.addBindContent(variantName + ".java#OUT_PORT", "mapResult");
		
		// Parameterize reduce slot depending on worker count.
		if(workers>1)
			cSys.addBindContent(variantName + ".java#REDUCE_SKEL", 
										"reduceResult = mIt.next();");	
		else
			cSys.addBind(variantName + ".java#REDUCE_SKEL", "simple_reduce.jbx");	
		
		// Bind and extend variant with reduce-dependent stuff.
		String reducePort = "mIt.hasNext()?mIt.next():null";
		cSys.addExtend(variantName + ".members", "reduce_op.jbx");
		cSys.addBindContent(variantName + ".java#IN_TYPE", mapType);
		cSys.addBindContent(variantName + ".java#OUT_TYPE", mapType);
		cSys.addBindContent(variantName + ".java#IN_PORT_INIT", reducePort);	
		cSys.addBindContent(variantName + ".java#IN_PORT_CONT", reducePort);
		cSys.addBindContent(variantName + ".java#OUT_PORT", "reduceResult");
		cSys.addBindContent(variantName + ".java#OUT_INIT", "new Hash" + mapType + "()");
		cSys.addBindTerminal(variantName + ".java#REDUCE_OP", "reduce");			
	}
	
	private void weaveDispatch(String variantName, int workers) throws ParseException, IOException {
		// Create dispatcher copy.
		String ifName = "dispatch" + workers + ".jbx";
		cSys.copyBox("dispatch.jbx", ifName);
		
		// Bind dispatcher condition depending on worker count.
		if(workers>1){
			cSys.addBindContent(ifName+"#CONDITION","cores>="+workers + "&& threshold< in.size()");		
		}
		else {
			cSys.addBindContent(ifName+"#CONDITION","cores== 1 || threshold >= in.size()");							
		}
		
		// Bind dispatch target and extend App.main. 
		cSys.addBindContent(ifName+"#VAR_NAME",variantName);
		cSys.addExtend("App.java#App.main.methodExit", ifName);
	}
	
	private void weaveImportComposers(String cuName) throws IOException {
		String pointName = cuName + "#imports";
		cSys.addExtendContent(pointName, "$ import java.util.Map;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.Callable;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.ExecutorService;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.Executors;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.Future;");
		cSys.addExtendContent(pointName, "$ import java.util.LinkedList;");
		cSys.addExtendContent(pointName, "$ import java.util.List;");
		cSys.addExtendContent(pointName, "$ import java.util.HashMap;");
	}
}
