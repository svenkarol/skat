/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;

import org.skat.binding.java.FragmentParser.ParseException;

import org.skat.binding.java.ast.ASTNode;
import org.skat.binding.java.ast.CompositionSystem;
import org.skat.binding.java.ast.List;
import org.skat.binding.java.ast.BlockBox;
import org.skat.binding.java.ast.CompilationUnitBox;
import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.binding.java.ast.ImportBox;
import org.skat.binding.java.ast.MethodBox;
import org.skat.binding.java.ast.QRef;

public class TestSkeletons extends TestBase{
	
	private static String VARIANT_PREFIX = "WikiCrawler";
	
	@Before
	public void setUp() throws IOException{
		doSetUp("count-and-sort/in","count-and-sort/out","count-and-sort/expected");
		System.out.println("Fragments in environment after setup: <" + cSys.getEnv().getFragments().getNumChild() + ">.");
	}
	
	@Test
	public void composeQueryCountMap() throws IOException{
		CompositionEnvironment env = cSys.getEnv();
		
		MethodBox mapOp1 = (MethodBox)env.findFragment("map_op.jbx_0");
		MethodBox mapOp2 = (MethodBox)env.findFragment("map_op.jbx_1");
		
		CompilationUnitBox computationBoxA = (CompilationUnitBox) env.findFragment("QueryCountMap.jbx");
		CompilationUnitBox mainBoxA = (CompilationUnitBox) env.findFragment("Main.jbx");
	
		// verify setup
		assertNotNull(computationBoxA);
		assertNotNull(mainBoxA);
		assertNotNull(mapOp1);
		assertNotNull(mapOp2);
		assertNotNull(env.findHook(new QRef("[[VAR_NAME]].members")));
		
		// adding composition operations for map-only variant
		composeMapVariant("A", "Query.getWikiPages(\"A\",100).values()", 2);
		composeMapVariant("B", "Query.getWikiPages(\"A\",100).values()", 1);
		
		composeMapReduceVariant("C", "Query.getWikiPages(\"A\",100).values()", 1);
		composeMapReduceVariant("D", "Query.getWikiPages(\"A\",100).values()", 2);

		//we use operator ordered composition to be faster
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION_FP);	
		
		System.out.println("Using Strategy " + cSys.getCompositionStrategy().getClass().getSimpleName() + ".");
		System.out.println("Triggering composition ... ");
		System.out.println("Fragments used for composition: <" + cSys.getEnv().getFragments().getNumChild() + ">.");
		System.out.println("Composers used for composition: <" + cSys.getEnv().getCompositionProgram().getComposers().getNumChild() + ">.");
		
		cSys.setRecoverMode(true);
		cSys.triggerComposition();
		
		cSys.printStatus();
		
		// persisting boxes with name Java only
		cSys.persistFragments(".*\\.java");
		
		compareExpected();
	}
	
	private void composeMapReduceVariant(String variantName, String queryExpression, int workers) throws IOException {
		String mainName = "Main" + variantName;
		String mapReduceName = VARIANT_PREFIX + variantName;

		cSys.copyBox("Main.jbx", mainName + ".java");
		cSys.copyBox("QueryCountMapReduce.jbx", mapReduceName + ".java");
		
		weaveImportComposers(mapReduceName + ".java");
		
		if(workers > 1){
			cSys.addBind(mapReduceName + ".java#MAP_SKEL", "concurrent_mapreduce.jbx");			
			cSys.addBindContent(mapReduceName + ".java#WORKERS", "(" + workers + ")");
		}
		else {
			cSys.addBind(mapReduceName + ".java#MAP_SKEL", "simple_map.jbx");			
		}
		
		weaveMapVariantComposers(mapReduceName, mainName, queryExpression, workers);
		
		if(workers>1){
			cSys.addBindContent(mapReduceName + ".java#REDUCE_SKEL", "reduceResult = mIt.next();");	
		}
		else{
			cSys.addBind(mapReduceName + ".java#REDUCE_SKEL", "simple_reduce.jbx");	
		}
		
		weaveReduceVariantComposers(mapReduceName, mainName, workers);
	}
	
	private void weaveDispatch(String variantSuffix, int workers) throws ParseException, IOException {
		String variantName = VARIANT_PREFIX + variantSuffix;
		String ifName = "dispatch" + variantSuffix + ".jbx";
		cSys.copyBox("dispatch.jbx", ifName);
		if(workers>1){
			cSys.addBindContent(ifName+"#CONDITION","resources()>="+workers + "&& threshold()< in.size()");		
		}
		else {
			cSys.addBindContent(ifName+"#CONDITION","resources()== 1 || threshold() >= in.size()");							
		}
		cSys.addBindContent(ifName+"#VAR_NAME",variantName);
		cSys.addExtend("App.java#App.main.methodExit", ifName);
	}
	
	private void weaveReduceVariantComposers(String mapReduceName, String mainName, int workers) throws ParseException, IOException {
		cSys.addExtend(mapReduceName + ".members", "reduce_op.jbx");
		
		cSys.addBindContent(mapReduceName + ".java#IN_TYPE", "Map<String, Integer>");
		cSys.addBindContent(mapReduceName + ".java#OUT_TYPE", "Map<String, Integer>");
		cSys.addBindContent(mapReduceName + ".java#IN_PORT_INIT", "mIt.hasNext()?mIt.next():null");	
		cSys.addBindContent(mapReduceName + ".java#IN_PORT_CONT", "mIt.hasNext()?mIt.next():null");
		cSys.addBindContent(mapReduceName + ".java#OUT_PORT", "reduceResult");
		cSys.addBindContent(mapReduceName + ".java#OUT_INIT", "new HashMap<String,Integer>()");
		cSys.addBindTerminal(mapReduceName + ".java#REDUCE_OP", "reduce");			
	}

	private void composeMapVariant(String variantName, String queryExpression, int workers) throws IOException{
		String mainName = "Main" + variantName ;
		String mapName = VARIANT_PREFIX + variantName ;
		
		cSys.copyBox("Main.jbx", mainName + ".java");
		cSys.copyBox("QueryCountMap.jbx", mapName + ".java");
		
		weaveImportComposers(mapName + ".java");
		
		if(workers > 1){
			cSys.addBind(mapName + ".java#MAP_SKEL", "concurrent_map.jbx");			
			cSys.addBindContent(mapName + ".java#WORKERS", "(" + workers + ")");
		}
		else {
			cSys.addBind(mapName + ".java#MAP_SKEL", "simple_map.jbx");			
		}
	
		weaveMapVariantComposers(mapName, mainName, queryExpression, workers);
	}
	
	private void weaveMapVariantComposers(String mapName, String mainName, String queryExpression, int workers) throws IOException {		
		cSys.addBindContent(mapName + ".java#VAR_NAME", mapName);
		cSys.addBindContent(mainName + ".java#VAR_NAME", mapName);
		cSys.addBindContent(mainName+ ".java#MAIN_NAME", mainName);

		cSys.addExtend(mapName + ".members", "map_op.jbx_0");
		cSys.addExtend(mapName + ".members", "map_op.jbx_1");
			
		cSys.addBindContent(mapName + ".java#IN_PORT_INIT", "it.hasNext()?it.next():null");		
		cSys.addBindContent(mapName + ".java#IN_PORT_CONT", "it.hasNext()?it.next():null");		
		cSys.addBindContent(mapName + ".java#IN_TYPE", "String");
		cSys.addBindTerminal(mapName + ".java#MAP_OP", "map");
		cSys.addBindContent(mapName + ".java#OUT_TYPE", "Map<String, Integer>");
		cSys.addBindContent(mapName + ".java#OUT_PORT", "mapResult");
		
		cSys.addBindContent(mainName + ".java#REQUEST_STMT", queryExpression);
	}
	
	private void weaveImportComposers(String cuName) throws IOException {
		String pointName = cuName + "#imports";
		cSys.addExtendContent(pointName, "$ import java.util.Map;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.Callable;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.ExecutorService;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.Executors;");
		cSys.addExtendContent(pointName, "$ import java.util.concurrent.Future;");
		cSys.addExtendContent(pointName, "$ import java.util.LinkedList;");
		cSys.addExtendContent(pointName, "$ import java.util.List;");
		cSys.addExtendContent(pointName, "$ import java.util.HashMap;");
	}
	
	@SuppressWarnings("rawtypes")
	//	@Test
	public void test() throws Exception {
		CompositionEnvironment env = cSys.getEnv();
		System.out.println("Fragments in environment: <" + env.getFragments().getNumChild() + ">.");
	    
		CompilationUnitBox classTpl = (CompilationUnitBox)env.findFragment("ClassTemplate.jbx");
		assertNotNull(classTpl);
		MethodBox qSort1 = (MethodBox)env.findFragment("quicksort.jbx_0");
		MethodBox qSort2 = (MethodBox)env.findFragment("quicksort.jbx_1");
		MethodBox qSort3 = (MethodBox)env.findFragment("quicksort.jbx_2");
		assertNotNull(qSort1);
		assertNotNull(qSort2);
		assertNotNull(qSort3);
		BlockBox bucketSort = (BlockBox)env.findFragment("bucketsort.jbx");
		assertNotNull(bucketSort);
		ImportBox import1 = (ImportBox)env.findFragment("i1");
		assertNotNull(import1);
		/*		for(TypeDecl decl:box.getFragment().getTypeDecls()){
			System.out.println(((ClassDecl)decl).getID());
			ClassDecl classDecl = (ClassDecl)decl;
			System.out.println(classDecl.getClass());
			for(BodyDecl bodyDecl:classDecl.getBodyDecls()){
				System.out.println(bodyDecl.getClass());
				if(bodyDecl instanceof MethodDecl){
					MethodDecl methodDecl = (MethodDecl)bodyDecl;
					System.out.println(methodDecl.getParameter(0).getTypeAccess());
				}
			}
		}*/
		
		
		assertNotNull(env.findSlot(new QRef("ClassNameSlot")));
		assertNotNull(env.findSlot(new QRef("TypeSlot")));
		assertNotNull(env.findSlot(new QRef("BUCKET_COUNT")));
		assertNotNull(env.findSlot(new QRef("LOCAL_SORT")));
		assertNotNull(env.findSlot(new QRef("ExpressionSlot")));
		assertNotNull(env.findSlot(new QRef("WorkerSlot")));
		assertNotNull(env.findSlot(new QRef("ResultTypeSlot")));
		assertNotNull(env.findSlot(new QRef("CallExpressionSlot")));		
		
		java.util.List<List> hooks = env.collHooks();
		System.out.println("Hooks in environment: <"+hooks.size()+">.");
		assertTrue(hooks.size()>0);		
		
		java.util.List<ASTNode> slots = env.collSlots();
		System.out.println("Slots in environment: <"+slots.size()+">.");
		
		/*
		for(int i=0;i<hooks.size();i++){
			AST.List hook = hooks.get(i);
			System.out.println(hook.DefaultHookQName());
		}
		*/
		List sortHook = env.findHook(new QRef("Main.reduce.methodEntry"));
		assertNotNull("Hook <Main.reduce.methodEntry> is missing.",sortHook);
		
		List importsHook = env.findHook(new QRef("ClassTemplate.jbx#imports"));
		assertNotNull(importsHook);
		
		cSys.addBind("ClassNameSlot","sortname");
		cSys.addBind("TypeSlot", "bucketDistType");
		cSys.addBind("ArgTypeSlot", "argType");
		cSys.addBind("BUCKET_COUNT", "bucketcount");
		cSys.addBind("LOCAL_SORT", "sortCall");
		cSys.addBind("ResultTypeSlot", "sortType");
		cSys.addBind("WorkerSlot", "workercount");
		cSys.addExtend("Sort.members","quicksort.jbx_0");
		cSys.addExtend("Sort.members","quicksort.jbx_1");
		cSys.addExtend("Sort.members","quicksort.jbx_2");
		cSys.addExtend("Sort.members","quicksort.jbx_3");
		cSys.addExtend("Sort.members", "reduce.jbx");
		cSys.addExtend("Sort.members", "map.jbx_0");
		cSys.addExtend("Sort.members", "map.jbx_1");
		cSys.addExtend("Sort.members", "bucketdistribution.jbx");	
		cSys.addExtend("Sort.doSort.*.methodExit", "iteratorInit");
		cSys.addExtend("Sort.doSort.*.methodExit", "mapResultDecl");
		cSys.addExtend("Sort.doSort.methodExit", "concurrent_map_template.jbx");
		cSys.addExtend("Sort.doSort.*.methodExit", "reduceResultDecl");
		cSys.addExtend("Sort.doSort.methodExit", "simple_reduce_template.jbx");
		cSys.addExtend("Sort.doSort.methodExit","bucketsort.jbx");
		cSys.addBind("ExpressionSlot","iteratorCheck");
		cSys.addBind("CallExpressionSlot","mapCall");
		cSys.addBind("PushResultSlot","mapResultPush");
		cSys.addExtend("Sort.doSort.block2.while3.statements", "iteratorCall");
		cSys.addBind("ResTypeSlot", "sortType");
		cSys.addBind("ArgInitSlot", "reduceArgInit");
		cSys.addBind("ResInitSlot", "reduceResInit");
		cSys.addBind("DoReduceSlot", "reduceOp");
		///adding imports
		cSys.addExtend("ClassTemplate.jbx#imports", "i1");
		cSys.addExtend("ClassTemplate.jbx#imports", "i2");
		cSys.addExtend("ClassTemplate.jbx#imports", "i3");
		cSys.addExtend("ClassTemplate.jbx#imports", "i4");
		cSys.addExtend("ClassTemplate.jbx#imports", "i5");
		cSys.addExtend("ClassTemplate.jbx#imports", "i6");
		cSys.addExtend("ClassTemplate.jbx#imports", "i7");
		cSys.addExtend("ClassTemplate.jbx#imports", "i8");
		cSys.addExtend("ClassTemplate.jbx#imports", "i9");
		cSys.addExtend("ClassTemplate.jbx#imports", "i10");

		
		
		System.out.println("Triggering composition.");
		long tCollectStart = System.currentTimeMillis();
		
		cSys.triggerComposition();
		
		assertNull(env.findSlot(new QRef("ClassNameSlot")));
		
		long tCollectEnd = System.currentTimeMillis();
		long tCollect = (tCollectEnd - tCollectStart)/1000;
		
		System.out.println("Composition execution time: <" + tCollect + "s>.");
		System.out.println("<" + slots.size() + "> slots ubounded.");
		
		//deactivating composers results in much better performance ... why?
		//potential reason: printing results in multiple tree traversals via fragment attributes (?) and triggers rewrites????
		//env.deactivateComposers();
		/*for(Box fragment:environment.getFragmentList()){
			if(fragment instanceof CompilationUnitBox){
				CompilationUnitBox cuBox = (CompilationUnitBox)fragment;
				System.out.println(cuBox.getName()+":"+cuBox.getFragment().getImportDeclList().getNumChild());
			}
		}*/
		classTpl = (CompilationUnitBox)env.findFragment("ClassTemplate.jbx");
		assertNotNull(classTpl.getFragment().getImportDeclList().getChild(0));
		System.out.println("Composers deactivated.");
		
		cSys.persistFragments();
		System.out.println("Fragments printed to <" +cSys.getOutURI()+ ">.");


	}
	
}
