/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;

public class TestStatementBug extends TestBase {

	@Before
	public void setUp() throws Exception {		
		doSetUp("statement-bug/in","statement-bug/out","statement-bug/expected");
	}	
	
	@Test
	public void testAddJobs() throws IOException{
		cSys.addFragment("compute_addJobs", "jobs.add(new PrimeNumbers(a[i],ret,i));");
		cSys.addBind("compute_addJobsSlot","compute_addJobs");
		cSys.triggerComposition();
		
		Assert.assertTrue(cSys.getEnv().findSlot("compute_addJobsSlot")==null);
		
		cSys.persistFragments(".+\\.jbx");
	}

}
