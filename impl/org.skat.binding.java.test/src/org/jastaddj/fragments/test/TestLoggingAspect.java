/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import static org.junit.Assert.*;

import org.skat.binding.java.FragmentParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.binding.java.ast.CompositionSystem;
import org.skat.binding.java.ast.JavaFragmentBox;

import org.skat.binding.java.ast.QRef;

public class TestLoggingAspect extends TestBase{


	@Before
	public void setUp() throws Exception {
		doSetUp("logging-aspect/in","logging-aspect/out","logging-aspect/expected");
		CompositionEnvironment env = cSys.getEnv();	
		JavaFragmentBox box = (JavaFragmentBox) FragmentParser.parseFragment(
			"$ \"decreasePrice\"", "string1", false);
		JavaFragmentBox box2 = (JavaFragmentBox) FragmentParser.parseFragment(
			"$ \"setPrice\"", "string2", false);
		JavaFragmentBox box3 = (JavaFragmentBox) FragmentParser.parseFragment(
				"private int field;", "field.jbx", false);
		assertNotNull(env.findFragment("log-statement.jbx"));
		JavaFragmentBox loggingCopy = (JavaFragmentBox) env
			.findFragment("log-statement.jbx").fullCopy();
		loggingCopy.setName("log-statement-copy.jbx");
		env.getFragments().add(box);
		env.getFragments().add(box2);
		env.getFragments().add(box3);
		assertNotNull(env.findFragment("field.jbx"));
		env.getFragments().add(loggingCopy);
	}
	
	@Test
	public void testAGComposition() throws Exception{
		cSys.clearCompositionProgram();
		cSys.setCompositionStrategy(CompositionSystem.AG_DEPTH_FIRST_COMPOSITION);
		test();
	}
	
	@Test
	public void testAGBFComposition() throws Exception{
		cSys.clearCompositionProgram();
		cSys.setCompositionStrategy(CompositionSystem.AG_BREADTH_FIRST_COMPOSITION);
		test();
	}
	
	@Test
	public void testOPComposition() throws Exception{
		cSys.clearCompositionProgram();
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION);
		test();
	}
	
	@Test
	public void testOPFPComposition() throws Exception{
		cSys.clearCompositionProgram();
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION_FP);
		test();
	}
	
	@Test
	public void testOPStrictComposition() throws Exception{
		cSys.clearCompositionProgram();
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION_STRICT);
		test();
	}
	
	@Test
	public void testPointComposition() throws Exception{
		cSys.clearCompositionProgram();
		cSys.setCompositionStrategy(CompositionSystem.POINT_ORDERED_COMPOSITION);
		test();
	}
	
	public void test() throws Exception {
		System.out.println("Now testing with algorithm '"+cSys.getCompositionStrategy().getClass().getSimpleName()+"'.");
		cSys.addBind("log-statement.jbx#methodName", "string1");
		cSys.addBind("log-statement-copy.jbx#methodName", "string2");
		cSys.addBind("decrease", "decrease.jbx");
		
		cSys.addExtend("Item.members","setter2.jbx");
		cSys.addExtend("Item.members","setter1.jbx");
		cSys.addExtend("Item.members","setter3.jbx");
		cSys.addExtend("Item.members","field.jbx");

		cSys.addExtend("Item.decreasePrice.methodEntry", "log-statement.jbx");
		cSys.addExtend("Item.setPrice.methodEntry", "log-statement-copy.jbx");
		
		assertNotNull(cSys.getEnv().findSlot(new QRef(
				"log-statement.jbx#methodName")));

		assertNull(cSys.getEnv().findHook(new QRef(
				"Item.decreasePrice.methodEntry")));

		cSys.triggerComposition();
		cSys.persistFragments();

		assertNotNull(cSys.getEnv().findHook(new QRef(
				"Item.decreasePrice.methodEntry")));
		assertNull(cSys.getEnv().findSlot(new QRef(
				"log-statement.jbx#methodName")));
		assertTrue(cSys.getEnv().isHealthy());
		
		compareExpected();

	}

}
