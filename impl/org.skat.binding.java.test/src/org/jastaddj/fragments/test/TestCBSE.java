/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.skat.binding.java.FragmentParser.ParseException;
import org.skat.binding.java.ast.ASTNode;
import org.skat.binding.java.ast.HookDescriptor;

public class TestCBSE extends TestBase {

	@Before
	public void setUp() throws Exception {
		doSetUp("cbse/in","cbse/out","cbse/expected");
	}

	//@Test
	public void testA() throws ParseException, IOException {
		cSys.addExtendContent("*.*.methodEntry", "System.out.println(\"Entering Method...\");");
		Assert.assertNotNull(cSys.getEnv().findHook("*.*.methodExit"));
		cSys.addExtendContent("*.*.methodExit", "System.out.println(\"Exiting Method...\");");
		cSys.triggerComposition();
		cSys.persistFragments();
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testB() throws IOException {
		for(HookDescriptor<ASTNode> hook:cSys.findHooks("*.*.methodEntry")){
			String prefix = hook.getContextPrefix();
			cSys.addExtendContent(prefix + ".methodEntry", "System.out.println(\"Entering Method '" + prefix + "'  ...\");");
			cSys.addExtendContent(prefix + ".methodExit", "System.out.println(\"Exiting Method '" + prefix + "'  ...\");");
		}
		Assert.assertNotNull(cSys.getEnv().findFragment("ServicePerformer.jbx"));
		Assert.assertEquals("ServicePerformer.jbx",cSys.getEnv().findFragment("ServicePerformer.jbx").resourceName());
		cSys.triggerComposition();
		cSys.persistFragments();
	}
	
	@Test
	public void testC() throws IOException{
		cSys.addBindContent("Control", "AccessControlService");
		cSys.addBindContent("ControlConstructor", "(new AccessControlService())");
		cSys.addBindContent("Check", "(ac.checkAccess())");
		
		//execute composers
		cSys.triggerComposition();
		
		//store fragments to out dir
		cSys.persistFragments();
		
		super.compareExpected();
	}

}
