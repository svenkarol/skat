/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Test;
import org.skat.binding.java.FragmentParser.ParseException;
import org.skat.binding.java.FragmentParser;
import org.skat.binding.java.ast.ASTNode;
import org.skat.binding.java.ast.AccessBox;
import org.skat.binding.java.ast.ArrayTypeAccessSlot;
import org.skat.binding.java.ast.BlockBox;
import org.skat.binding.java.ast.CompilationUnitBox;
import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.binding.java.ast.ConstructorBox;
import org.skat.binding.java.ast.ExprSlot;
import org.skat.binding.java.ast.ExpressionBox;
import org.skat.binding.java.ast.FieldDeclNameSlot;
import org.skat.binding.java.ast.GenericTypeAccessSlot;
import org.skat.binding.java.ast.ImportBox;
import org.skat.binding.java.ast.JavaFragmentBox;
import org.skat.binding.java.ast.MemberBox;
import org.skat.binding.java.ast.MemberDeclSlot;
import org.skat.binding.java.ast.MethodBox;
import org.skat.binding.java.ast.DotBox;
import org.skat.binding.java.ast.ParameterDeclNameSlot;
import org.skat.binding.java.ast.StatementBox;
import org.skat.binding.java.ast.StmtSlot;
import org.skat.binding.java.ast.TerminalSlot;
import org.skat.binding.java.ast.TypeAccessSlot;
import org.skat.binding.java.ast.TypeVariableSlot;
import org.skat.binding.java.ast.VarAccessSlot;


/**
 * Test cases to check if the fragment parser works as expected. "testUnprefixedParse()" and 
 * "testAnnotatedFragments()" check cases for plain fragment parsing, without any slots for
 * most of the available fragment types (specified in fragment.parser in the main project).
 */
public class TestFragmentParser extends TestBase {
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testAnnotatedFragments() throws IOException {
		try {
			ASTNode result = FragmentParser.parseFragment(
					"STMT::= a=a+b*c;", "f1", false);
			Assert.assertTrue(result instanceof StatementBox);

			result = FragmentParser.parseFragment("LITERAL::=\"test\"", "f2", false);
			Assert.assertTrue(result instanceof ExpressionBox);

			result = FragmentParser.parseFragment(
					"EXPRESSION::=5 - 6 % 5 + (\"test\")", "f3", false);
			Assert.assertTrue(result instanceof ExpressionBox);

			//result = FragmentParser.parseFragment("TYPE::=AClass", "f4", false);
			//Assert.assertTrue(result instanceof TypeAccessBox);

			result = FragmentParser.parseFragment(
					"IMPORT::=import a.b.packagename.AClass;", "f5", false);
			Assert.assertTrue(result instanceof ImportBox);

			result = FragmentParser.parseFragment(
					"FIELD::=private static final type a;", "f6", false);
			Assert.assertTrue(result instanceof MemberBox);

			result = FragmentParser.parseFragment(
					"VARIABLE::=type name = expression;", "f7", false);
			Assert.assertTrue(result instanceof StatementBox);

			result = FragmentParser.parseFragment(
					"BLOCK::= { test(); test2(); a=3; }", "f8", false);
			Assert.assertTrue(result instanceof BlockBox);

			result = FragmentParser.parseFragment(
					"METHOD::= public void myMethod(String b){test();}", "f9", false);
			Assert.assertTrue(result instanceof MethodBox);
			
			result = FragmentParser.parseFragment(
					"NAME::= a.bd.asda", "f10", false);
			Assert.assertTrue(result instanceof DotBox);
			
			result = FragmentParser.parseFragment(
					"TYPE::= ASTNode<A>", "f11", false);
			Assert.assertTrue(result instanceof AccessBox);	
			
			result = FragmentParser.parseFragment(
					"NAME::=abf", "f12", false);
			System.out.println(result.getClass().getName());
			Assert.assertTrue(result instanceof AccessBox);
			
			result = FragmentParser.parseFragment(
					"STMT::=int x;", "f13", false);
			Assert.assertTrue(result instanceof StatementBox);

			
		} catch (ParseException e) {
			Assert.fail(e.getDetailedMessage());
		}
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testUnprefixedParse() throws IOException{
		try {
			ASTNode result = FragmentParser.parseFragment("protected static void methodM(Boolean b){blah(); a=4*7;}", "f1", true);
			Assert.assertTrue(result instanceof MethodBox);
			
			result = FragmentParser.parseFragment("private static final type.type.type a;", "f2", true);
			Assert.assertTrue(result instanceof MemberBox);
			
			//parsing an import like this yields a compilation unit
			result = FragmentParser.parseFragment("import pck.name.Test;", "f3", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			
			result = FragmentParser.parseFragment("{a(); c = 1+3/4;}","f4", true);
			Assert.assertTrue(result instanceof BlockBox);
			
			result = FragmentParser.parseFragment("\"Test\"","f5", true);
			Assert.assertTrue(result instanceof ExpressionBox);
			
			result = FragmentParser.parseFragment("ba.b.c","f6", true);
			Assert.assertTrue(result instanceof DotBox);
			
			result = FragmentParser.parseFragment("ASTNode<List<A,B>>","f7", true);
			Assert.assertTrue(result instanceof AccessBox);
			
			result = FragmentParser.parseFragment("int","f8", true);
			Assert.assertTrue(result instanceof AccessBox);
			
			result = FragmentParser.parseFragment("int[][]","f9", true);
			Assert.assertTrue(result instanceof AccessBox);
			
			result = FragmentParser.parseFragment("new Test();","f10",true);
			Assert.assertTrue(result instanceof StatementBox);
			
			result = FragmentParser.parseFragment("a+b+c", "f11", true);
			Assert.assertTrue(result instanceof ExpressionBox);
			
			result = FragmentParser.parseFragment("(new Test())", "f12", true);
			Assert.assertTrue(result instanceof ExpressionBox);
			
			result = FragmentParser.parseFragment("public class A extends B implements C { public A(String name){return name;} }", "f13",true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			
			result = FragmentParser.parseFragment("public interface A {}", "f14", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			
			
		} catch (ParseException e) {
			Assert.fail(e.getDetailedMessage());
		}
	
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testSlotParse() throws IOException{
		try{
			// Dummy environment to provide context.
			CompositionEnvironment env = new CompositionEnvironment();
			
			ASTNode result = FragmentParser.parseFragment("public class A extends B implements C { public A(String name){[[StmtSlot]]; return name;} }", "f1", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			JavaFragmentBox box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(!box.collSlots().isEmpty());
			ASTNode slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof StmtSlot);
			Assert.assertEquals("StmtSlot", slot.slotName());
			
			result = FragmentParser.parseFragment("public class A extends B implements C { public A([[TypeAccess]] name){return name;} }", "f2", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(!box.collSlots().isEmpty());
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("TypeAccess", slot.slotName());
			
			result = FragmentParser.parseFragment("public class A extends B implements C { public A([[ArrayAccess]][] name){return name;} }", "f3", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof ArrayTypeAccessSlot);
			Assert.assertEquals("ArrayAccess", slot.slotName());
			
			result = FragmentParser.parseFragment("public class A implements C { public A(int name){return name;} }", "f4", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(!box.collSlots().isEmpty());
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("ImplicitSuperClass", slot.slotName());
			
			result = FragmentParser.parseFragment("public class [[A]] extends B implements C { public A(int name){return name;} }", "f5", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TerminalSlot);
			Assert.assertEquals("A", slot.slotName());
			
			result = FragmentParser.parseFragment("public static void compute(Object array){ [[TAccess]][] acc = new AClass[2]; }", "f6", true);
			Assert.assertTrue(result instanceof MethodBox);
			box = (MethodBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof ArrayTypeAccessSlot);
			Assert.assertEquals("TAccess", slot.slotName());
			
			result = FragmentParser.parseFragment("public static void compute(Object array){ AClass acc = new [[TAccess]](); }", "f7", true);
			Assert.assertTrue(result instanceof MethodBox);
			box = (MethodBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("TAccess", slot.slotName());
			
			result = FragmentParser.parseFragment("public static void compute(Object array){ AClass acc = new [[TAccess]][2]; }", "f8", true);
			Assert.assertTrue(result instanceof MethodBox);
			box = (MethodBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("TAccess", slot.slotName());
			
			result = FragmentParser.parseFragment("public static void compute(Object array){ AClass acc = new [[TAccess]][[[IExp]]]; }", "f9", true);
			Assert.assertTrue(result instanceof MethodBox);
			box = (MethodBox) result;
			Assert.assertTrue(box.collSlots().size()==2);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("TAccess", slot.slotName());
			slot =  box.collSlots().get(1);
			Assert.assertTrue(slot instanceof ExprSlot);
			Assert.assertEquals("IExp", slot.slotName());
			
			result = FragmentParser.parseFragment("public static void compute(T<[[PAR]]> array){ AClass acc = new AClass(); }", "f10", true);
			Assert.assertTrue(result instanceof MethodBox);
			box = (MethodBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof GenericTypeAccessSlot);
			Assert.assertEquals("PAR",slot.slotName());
			
			result = FragmentParser.parseFragment("public class A <[[PAR]]> extends B { public A(String name){return name;} }", "f11", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeVariableSlot);
			Assert.assertEquals("PAR",slot.slotName());
			
			result = FragmentParser.parseFragment("public interface [[INAME]] {}", "f12", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertEquals("INAME", slot.slotName());
			Assert.assertTrue(slot instanceof TerminalSlot);
			
			result = FragmentParser.parseFragment("public static void [[METHODNAME]](Object array){  }", "f13", true);
			Assert.assertTrue(result instanceof MethodBox);
			box = (MethodBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TerminalSlot);
			Assert.assertEquals("METHODNAME", slot.slotName());

			result = FragmentParser.parseFragment("public class CONSNAME { public [[CONSNAME]](){  } }", "f14", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			Assert.assertTrue(box.collSlots().size()==2);
			slot =  box.collSlots().get(1);
			Assert.assertTrue(slot instanceof TerminalSlot);
			Assert.assertEquals("CONSNAME", slot.slotName());
			
			result = FragmentParser.parseFragment("public [[CONSNAME]](){  }", "f15", true);
			Assert.assertTrue(result instanceof ConstructorBox);
			box = (ConstructorBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TerminalSlot);
			Assert.assertEquals("CONSNAME", slot.slotName());
			
			result = FragmentParser.parseFragment("{ final String a = [[ACCESSNAME]](); }", "f16", true);
			Assert.assertTrue(result instanceof BlockBox);
			box = (BlockBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TerminalSlot);
			Assert.assertEquals("ACCESSNAME", slot.slotName());
			
			result = FragmentParser.parseFragment("{ a(); [[STMTNAME]]; }", "f17", true);
			Assert.assertTrue(result instanceof BlockBox);
			box = (BlockBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof StmtSlot);
			Assert.assertEquals("STMTNAME", slot.slotName());
			
			result = FragmentParser.parseFragment("{ a(); a = [[EXPRNAME]]; }", "f18", true);
			Assert.assertTrue(result instanceof BlockBox);
			box = (BlockBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof ExprSlot);
			Assert.assertEquals("EXPRNAME", slot.slotName());

			result = FragmentParser.parseFragment("public class A extends B implements C { [[MemberSlot]]; public A(String name){ return name;} }", "f19", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(!box.collSlots().isEmpty());
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof MemberDeclSlot);
			Assert.assertEquals("MemberSlot", slot.slotName());
			
			result = FragmentParser.parseFragment("public class A extends B implements C { private [[Type]] [[Name]]; public A(String name){ return name;} }", "f20", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==2);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("Type", slot.slotName());
			slot = box.collSlots().get(1);
			Assert.assertTrue(slot instanceof FieldDeclNameSlot);
			Assert.assertEquals("Name", slot.slotName());
			
			result = FragmentParser.parseFragment("public class A extends B implements C { public A([[Type]] [[Name]]){ return name;} }", "f21", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==2);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("Type", slot.slotName());
			slot = box.collSlots().get(1);
			Assert.assertTrue(slot instanceof ParameterDeclNameSlot);
			Assert.assertEquals("Name", slot.slotName());
			
			result = FragmentParser.parseFragment("public class A extends [[SuperClass]] implements [[SuperInterface]] { }", "f22", true);
			Assert.assertTrue(result instanceof CompilationUnitBox);
			box = (CompilationUnitBox) result;
			env.addFragment(box);
			Assert.assertTrue(box.collSlots().size()==2);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("SuperClass", slot.slotName());
			slot =  box.collSlots().get(1);
			Assert.assertTrue(slot instanceof TypeAccessSlot);
			Assert.assertEquals("SuperInterface", slot.slotName());
			
			result = FragmentParser.parseFragment("{ this.[[VARNAME]] = true; }", "f23", true);
			Assert.assertTrue(result instanceof BlockBox);
			box = (BlockBox) result;
			Assert.assertTrue(box.collSlots().size()==1);
			slot =  box.collSlots().get(0);
			Assert.assertTrue(slot instanceof VarAccessSlot);
			Assert.assertEquals("VARNAME", slot.slotName());
		}
		catch(ParseException e){
			Assert.fail(e.getDetailedMessage());
		}
	}

}
