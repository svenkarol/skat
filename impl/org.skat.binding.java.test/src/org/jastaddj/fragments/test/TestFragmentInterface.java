/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.jastaddj.fragments.test;

import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;
import org.junit.Before;

import org.skat.binding.java.FragmentParser.ParseException;
import org.skat.binding.java.ast.ASTNode;
import org.skat.binding.java.ast.Box;
import org.skat.binding.java.ast.CompositionSystem;
import org.skat.binding.java.ast.Dot;
import org.skat.binding.java.ast.DotBox;
import org.skat.binding.java.ast.Extract;
import org.skat.binding.java.ast.List;
import org.skat.binding.java.ast.Bind;
import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.binding.java.ast.Extend;
import org.skat.binding.java.ast.QRef;

public class TestFragmentInterface extends TestBase{
	
	@Before
	public void setUp() throws IOException {
		doSetUp("test-fragments/in","test-fragments/out","test-fragments/expected");
	}

	@SuppressWarnings({ "rawtypes", "static-access" })
	@Test
	public void test() throws Exception {
		CompositionEnvironment env = cSys.getEnv();
			
		assertEquals(4,env.getFragments().getNumChild());
		assertNotNull(env.getBytecodeReader());
		
		QRef qRef = new QRef("Device.members");
		
		assertTrue(qRef.isCorrect());

		System.out.println("Found the following boxes:");
		for(Box box:env.getFragmentList()){
			System.out.println("\tName: "+box.getName() + ", Type: "+box.getClass().getSimpleName());
			if("method.jbx".equals(box.getName())){
				assertEquals("MethodBox",box.getClass().getSimpleName());
			}
			else if("statement.jbx".equals(box.getName())){
				assertEquals("StatementBox",box.getClass().getSimpleName());
			}
			else if("Device.java".equals(box.getName())){
				assertEquals("CompilationUnitBox",box.getClass().getSimpleName());
				assertEquals("org/devices/Device.java",box.resourceName());
				box.setOutName("myorg/mydevice/MyDevice.java");
				assertEquals("myorg/mydevice/MyDevice.java",box.resourceName());
				box.setOutName("");
				assertEquals("org/devices/Device.java",box.resourceName());
			}
			else if("field.jbx".equals(box.getName())){
				assertEquals("MemberBox",box.getClass().getSimpleName());
			}

		}
		
		System.out.println("Found the following hooks:");
		for(List hook:env.collHooks()){
			System.out.print("\t"+hook.pointPrefix()+"-->"+hook.hookName());
			if(hook.hookAliases().length>0){
				System.out.print(" (Aliases:");
				for(int i=0;i<hook.hookAliases().length;i++){
					System.out.print(" " + hook.hookAliases()[i]);
				}
				System.out.print(")");
			}
			System.out.println();
		}
		assertEquals(21,env.collHooks().size());
		
		List declHook = env.findHook(new QRef("Device.members"));
		assertTrue("Device.members".equals(qRef.pointName()));
		assertNotNull(declHook);
		List implementsHook = env.findHook(new QRef("Device.implements"));
		assertNotNull(implementsHook);
		List testMethodHook = env.findHook(new QRef("Device.testMethod.methodEntry"));
		assertNotNull(testMethodHook);
		List returnMethodHook = env.findHook(new QRef("Device.returnHooks.*.methodExit"));
		assertNotNull(returnMethodHook);
		List testMethodExitHook0 = env.findHook(new QRef("Device.returnHooks.*.methodExit1"));
		assertNotNull(testMethodExitHook0);
		List testMethodExitHook1 = env.findHook(new QRef("Device.returnHooks.*.methodExit4"));
		assertNotNull(testMethodExitHook1);
		List consHook = env.findHook(new QRef("Device.Device.statements"));
		assertNotNull(consHook);
		List methodMultipleHook1 = env.findHook(new QRef("Device.multiple().statements"));
		assertNotNull(methodMultipleHook1);
		List methodMultipleHook2 = env.findHook(new QRef("Device.multiple(*String).statements"));
		assertNotNull(methodMultipleHook2);
		assertTrue(methodMultipleHook1!=methodMultipleHook2);
		List methodMultipleHook3 = env.findHook(new QRef("Device.multiple(float).statements"));
		assertNotNull(methodMultipleHook3);
		assertTrue(methodMultipleHook2!=methodMultipleHook3);
		assertTrue(methodMultipleHook1!=methodMultipleHook3);
		
		List testHook = env.findHook(new QRef("test.methodExit"));
		assertNotNull(testHook);
		java.util.List<ASTNode> slots = env.collSlots();
		/*debug:
		for(ASTNode node:slots){
			System.out.println(node.slotName()+node.hashCode());
		}*/
		assertEquals(3,slots.size());
		assertNotNull(env.findSlot("ImplicitSuperClass"));
		assertNotNull(env.findSlot("test"));
		assertNotNull(env.findSlot("rudiment"));
		
		Extend extend = new Extend();
		extend.setFragmentName("statement.jbx");
		extend.setPointName(new QRef("Device.java#Device.testMethod.methodEntry"));
			
		Bind bind = new Bind();
		bind.setFragmentName("method.jbx");
		bind.setPointName(new QRef("test"));
		
		env.getCompositionProgram().addComposer(extend);
		env.getCompositionProgram().addComposer(bind);
		
		assertNotNull(bind.targetPoint());
		assertNotNull(bind.srcFragment());
		assertTrue(bind.getPointName().isCorrect());
		assertTrue("test".equals(bind.getPointName().pointName()));
		assertNull(bind.getPointName().fragmentName());
		assertNotNull(bind.targetPoint());
		assertTrue(bind.targetPoint().isSlot());
		
		assertNotNull(extend.targetPoint());
		assertNotNull(extend.srcFragment());
		
		ASTNode extendPoint = extend.targetPoint(); 
		
		//by default, slots are rudiments
		ASTNode rudiment = env.findRudiment("*.rudiment");
		assertNotNull(rudiment);
		Extract extract = cSys.createExtract("*.rudiment", null);
		env.getCompositionProgram().addComposer(extract);

		assertTrue(rudiment.hasAssociatedComposer());
		
		assertNotNull(extract.targetPoint());
		
		cSys.setCompositionStrategy(CompositionSystem.POINT_ORDERED_COMPOSITION);
		cSys.triggerComposition();
		cSys.persistFragments();

		//rewrite was executed
		assertNotNull(extend.srcFragment());
		assertTrue(extend.isExhausted(extendPoint));
		
		//fragment still available
		assertNotNull(bind.srcFragment());
		
		//slot was replaced, hence Slot() should return null after collection flush
	    env.flushCollectionCache();
		assertNull(bind.targetPoint());
		
		assertNull(env.findRudiment("*.rudiment"));		
	}
	
	@SuppressWarnings("rawtypes")
	@Test
	public void testDot() throws ParseException, IOException{
		cSys.addFragment("Dots","java.util.Date");
		Box box = cSys.findFragment("Dots");
		assertTrue(box instanceof DotBox);
		ASTNode fragment = box.fragment();
		// check for accidental reclassification (e.g., if the qname is in classpath it may be resolved to TypeAccess)
		// see also "eq DotBox.fragment()" in Fragments.jrag
		assertTrue(fragment instanceof Dot);
	    Box copy = (Box)box.fullCopy();
	    assertTrue(copy.fragment() instanceof Dot);
	}

}
