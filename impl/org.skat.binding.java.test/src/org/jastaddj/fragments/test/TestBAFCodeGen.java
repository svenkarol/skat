package org.jastaddj.fragments.test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.LinkedHashSet;
import java.util.List;

import junit.framework.Assert;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.emftext.language.businessmodel.BusinessModel;
import org.emftext.language.businessmodel.PropertyDefinition;
import org.emftext.language.businessmodel.RoleDefinition;
import org.emftext.language.businessmodel.resource.bm.mopp.BmResourceFactory;
import org.junit.Before;
import org.junit.Test;
import org.skat.binding.java.ast.CompositionSystem;

public class TestBAFCodeGen extends TestBase {

	@Before
	public void setUp() throws Exception {
		doSetUp("business-application-framework/in","business-application-framework/out","business-application-framework/expected");
	}
	
	@Test
	public void runCompositionProgram() throws IOException{
		compositionProgram(new File("test-src/business-application-framework/in/model.bm"));
	}
	
	public void compositionProgram(File modelFile) throws IOException {
		// Load the BusinessModel object with EMFText.
		BusinessModel bm = loadBusinessModel(modelFile);
		
		// Configure the composition system.
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION_FP);
		cSys.setRecoverMode(true);
		long t0_0 = System.currentTimeMillis();
		
		// For each role definition, generate a Java class.
		for(RoleDefinition role: preOrder(bm.getRoleDefinitions())){
			
			// Instantiate template Person.jbx for each role.
			String cuName = role.getName() + ".java";
			cSys.copyBox("Person.jbx",cuName);
			cSys.addBindContent(cuName+"#Type",role.getName());
			cSys.addBindContent(cuName+"#TypeName","\"" + role.getName() + "\"");
			cSys.addBindContent(cuName+"#ImplicitSuperClass","Person");
			cSys.addExtendContent(cuName+"#*.membersEntry","public [[Type]](){super();}");
			cSys.addBindTerminal(cuName+"#Pfx","\n");
			cSys.triggerComposition();
			cSys.clearCompositionProgram();
			
			// Mix in code of super roles.
			for(RoleDefinition superRole:role.getSuperRoles()){
				mixin(role.getName() + ".java",superRole.getName() + ".java");
			}
			
			// Generate code for PropertyDefinitions of the current role.
			for(PropertyDefinition def:concat(role.getProperties(),getSuperProps(role))){
				
				// If there's not already a mixed-in implementation, add members.
				if(def.eContainer()==role && !isShadowed(def)){
					cSys.addExtend(cuName+"#*.members","Setter.jbx");
					cSys.addExtend(cuName+"#*.members","Getter.jbx");
					cSys.addExtend(cuName+"#*.members","Field.jbx");
					cSys.addBindContent(cuName+"#Type",def.getType().getTargetType());
					cSys.addBindContent(cuName+"#SetSfx","set"+toFirstUpper(def.getName()));
				}
				
				// Extend asString().
				if(def.eContainer()==role && !isShadowed(def) || def.eContainer()!=role){
					String stmt = "v += \"\\n [[Field]]:\" + [[GetSfx]]();";
					cSys.addExtendContent(cuName + "#*.asString.methodExit",stmt);
					cSys.addBindContent(cuName + "#GetSfx","get" + toFirstUpper(def.getName()));
					cSys.addBindContent(cuName + "#Field",def.getName());
				}
				
				// Extend constructor with defaults.
				if(def.eContainer()==role && def.getType().getDefault()!=null){
					String stmt = def.getName() + "=" + def.getType().getDefault() + ";";
					cSys.addExtendContent(cuName + "#*." + role.getName() + ".statements", stmt);
				}		
				cSys.triggerComposition();
				cSys.clearCompositionProgram();	
			}				
	    }

		Assert.assertTrue(cSys.getEnv().isHealthy());
		cSys.persistFragments();
	    long t0_1 = System.currentTimeMillis();
	    System.out.println("Composition took " + (t0_1-t0_0) + "ms.");
	    compareExpected();
	}
	
	// Pre-order inheritance chain for mixin operator (super roles need to be processed first).
	public Collection<RoleDefinition> preOrder(List<RoleDefinition> defs){
		defs = new LinkedList<RoleDefinition>(defs);
		LinkedHashSet<RoleDefinition> newOrder = new LinkedHashSet<RoleDefinition>(); 
		while(!defs.isEmpty()){
			Iterator<RoleDefinition> it = defs.iterator();
			while(it.hasNext()){
				RoleDefinition role = it.next();
				boolean allProcessed = true;
				for(RoleDefinition superRole:role.getSuperRoles()){
					if(!newOrder.contains(superRole)){
						allProcessed = false;
						break;
					}
				}
				if(allProcessed){
					newOrder.add(role);
					it.remove();
				}
			}
		}
		return newOrder;
	}
	
	public BusinessModel loadBusinessModel(File file){
		return (BusinessModel)loadModel(file);	
	}
	
	public EObject loadModel(File file){
		URI emfURI = URI.createURI(file.toURI().toString());
		ResourceSet rs = new ResourceSetImpl();
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put(
			"bm", new BmResourceFactory()
		);		
		Resource resource = rs.getResource(emfURI, true);
		EObject o = resource.getContents().get(0);
		return o;
	}
	
	private String toFirstUpper(String in){
		String out = in.substring(0,1).toUpperCase();
		out += in.substring(1);
		return out;
	}
	
	public List<PropertyDefinition> concat(List<PropertyDefinition> left,List<PropertyDefinition> right){
		List<PropertyDefinition> list = new LinkedList<PropertyDefinition>(left);
		list.addAll(right);
		return list;
	}
	
	private boolean isShadowed(PropertyDefinition property){
		RoleDefinition owner = (RoleDefinition)property.eContainer();
		for(RoleDefinition superRole:owner.getSuperRoles()){
			for(PropertyDefinition superProperty:superRole.getProperties()){
				if(superProperty.getName().equals(property.getName())){
					return true;
				}
			}
		}
		return false;
	}
	
	private List<PropertyDefinition> getSuperProps(RoleDefinition owner){
		List<PropertyDefinition> superProps = new LinkedList<PropertyDefinition>();
		for(RoleDefinition superRole:owner.getSuperRoles()){
			superProps.addAll(superRole.getProperties());
		}
		return superProps;
	}
	
	private void mixin(String target, String mixin) throws IOException{
		TestMixin.mixin(target, mixin, cSys);
		/*ClassBox classBox  = ((CompilationUnitBox) cSys.getEnv().findFragment(mixin)).extractClassBox();
		cSys.addExtendContent(mixin+"#*.implements", "I"+classBox.getFragment().getID());
		cSys.triggerComposition();*/
	}
}
