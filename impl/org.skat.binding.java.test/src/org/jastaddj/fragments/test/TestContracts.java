package org.jastaddj.fragments.test;

import java.io.IOException;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Test;
import org.skat.binding.java.ast.Box;
import org.skat.binding.java.ast.CompositionSystem;

public class TestContracts extends TestBase {
	@Before
	public void setUp() throws IOException {
		doSetUp("test-contracts/in","test-contracts/out","test-contracts/expected");
	}

	@Test
	public void test() throws Exception {
		cSys.setCompositionStrategy(CompositionSystem.OP_ORDERED_COMPOSITION);
		cSys.setRecoverMode(true);
		cSys.addExtendContent("Item.members","public void init(){bar=\"foo\";}");
		cSys.addExtendContent("Item.members","public void init(){this.name=\"foo\";}");
		cSys.addExtendContent("Item.members","public void init(){this.name=\"bar\";}");
		cSys.addExtendContent("Item.members","public String toString(){return \"foo\";}");
		cSys.addExtendContent("Item.members","public int a;");
		cSys.addBindContent("PriceSlot", "true+3");
		cSys.addBindContent("OtherSlot", "price");
		Box box = cSys.getEnv().findFragment("~jSkAT_ID6");
		Assert.assertTrue(box.fragment().getClass().getSimpleName().equals("ParseName"));
		cSys.triggerComposition();
		cSys.persistFragments();
		compareExpected();
		cSys.printStatus();
		Assert.assertTrue(cSys.getEnv().status.size()==7);
	}
}
