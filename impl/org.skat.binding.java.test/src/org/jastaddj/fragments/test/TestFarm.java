/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 */
package org.jastaddj.fragments.test;

import org.skat.binding.java.FragmentParser;
import org.junit.Before;
import org.junit.Test;

import org.skat.binding.java.ast.CompositionEnvironment;
import org.skat.binding.java.ast.JavaFragmentBox;

public class TestFarm extends TestBase{
	
		
		@Before
		public void setUp() throws Exception {
			doSetUp("test-farm/in","test-farm/out","test-farm/expected");

		// Global Variables
		    JavaFragmentBox gbv_box0 = (JavaFragmentBox) FragmentParser.parseFragment("final static int workers =4;", "var_workers.jbx",false);
		    JavaFragmentBox gbv_box1 = (JavaFragmentBox) FragmentParser.parseFragment("final static Object[] globalSourceArray = {10,100,101,200,201,300,301,1000000};", "var_srcArray.jbx",false);
		    JavaFragmentBox gbv_box2 = (JavaFragmentBox) FragmentParser.parseFragment("final static int row =2;", "var_row.jbx",false);
		    JavaFragmentBox gbv_box3 = (JavaFragmentBox) FragmentParser.parseFragment("final static int row = 2 ;", "var_column.jbx",false);
			
		    CompositionEnvironment env = cSys.getEnv();
			env.getFragments().add(gbv_box0);
			env.getFragments().add(gbv_box1);
			env.getFragments().add(gbv_box2);
			env.getFragments().add(gbv_box3);
			
			
			//fragments for compute 
			JavaFragmentBox com_box0 = (JavaFragmentBox) FragmentParser.parseFragment("$ Integer[][]", "compute_inGPU_srcArrayType",false);
			JavaFragmentBox com_box1 = (JavaFragmentBox) FragmentParser.parseFragment("$ Integer[][]", "compute_inGPU_retArrayType",false);
			//JavaFragmentBox com_box2 = (JavaFragmentBox) FragmentParser.parseFragment("$ Integer [2][100]", "compute_inGPU_retArrayInit",false);
			
			env.getFragments().add(com_box0);
			env.getFragments().add(com_box1);
			//env.getFragments().add(com_box2);
			
			JavaFragmentBox box5 = (JavaFragmentBox) FragmentParser.parseFragment("$ \"main\"", "farm_mainMethodName",false);
			env.getFragments().add(box5);
			
			cSys.addFragment("app_class", "PrimeNumbers");
			//compositionSystem.addFragment("compute_inGPU_srcArrayType", "$ Integer");
			//compositionSystem.addFragment("compute_inGPU_retArrayType", "Integer []");
			cSys.addFragment("farm_mainMethod" ,"main");
			cSys.addFragment("compute_inGPU_retArrayInit" ,"new Integer [row][100]");
			//JavaFragmentBox box2 = (JavaFragmentBox) FragmentParser.parseFragment("$ String ", "retType.jbx");
			//compositionSystem.addFragment("retType.jbx","$ \"String[]\"" );
			
		}
		
		
		
		@Test
		public void test() throws Exception {
			
			
			
			//composing global var
			cSys.addExtend("Farm.members","var_workers.jbx");
			cSys.addExtend("Farm.members","var_srcArray.jbx");
			
			//composing slots in main
			cSys.addBind("main.jbx#methodName", "farm_mainMethodName");
			
			//composing slots in compute		         
			cSys.addBind("compute_inGPU.jbx#compute_inGPU_srcArrayType", "compute_inGPU_srcArrayType");
			cSys.addBind("compute_inGPU.jbx#compute_inGPU_retArrayType", "compute_inGPU_retArrayType");
			
			
			cSys.addBind("compute_inGPU_srcArrayTypeSlot","compute_inGPU_srcArrayType");
			cSys.addBind("compute_inGPU_retArrayTypeSlot","compute_inGPU_retArrayType");
			cSys.addBind("compute_inGPU_retArrayInitSlot","compute_inGPU_retArrayInit");
			cSys.addBind("compute_ClassSlot","app_class");
			
			cSys.addBind("Farm.java#main", "main.jbx");
			cSys.addBind("Farm.java#spliting", "split_matrix.jbx");
			cSys.addBind("Farm.java#computing", "compute_inGPU.jbx");		
			cSys.addBind("Farm.java#joining", "join_matrixToArray.jbx");
			
			
			cSys.triggerComposition();
			cSys.persistFragments();
		}

	}