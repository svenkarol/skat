/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */
package org.skat.isc.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;

/**
 * A place to put File handling operations that are needed in all derived SkAT projects.
 */
public final class IOHelper {
	
	public static String readFile(File file) throws java.io.IOException {
		java.io.Reader reader = new java.io.BufferedReader(
				new java.io.FileReader(file));
		return readStream(reader);
	}
	
	public static String readFile(File file, String encoding) throws java.io.IOException {
		if(encoding!=null){
			InputStream in = new FileInputStream(file);	
			java.io.Reader reader = new java.io.BufferedReader(
					new InputStreamReader(in,Charset.forName(encoding)));
			return readStream(reader);			
		}
		else{
			return readFile(file);
		}
	}
	
	public static String readStream(Reader reader) throws java.io.IOException {
		StringBuffer buf = new StringBuffer();
		char[] cbuf = new char[1024];
		int i = 0;
		while ((i = reader.read(cbuf)) != -1)
			buf.append(String.valueOf(cbuf, 0, i));
		reader.close();
		return buf.toString();
	}
	
	public static String readStream(InputStream stream) throws java.io.IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
		return readStream(reader);
	}
	
	/**
	 * Given a Collection of file handlers and an array of file endings as filter strings 
	 * this methods filters the given file handlers according to their file endings. If 
	 * a File object points to a directory, the method fill also recursively inspect that 
	 * directory and apply the file ending filter. An empty filter array will suppress 
	 * filter application. 
	 * 
	 * @param files
	 * @param filter
	 * @return A collection of files with the filtered endings.
	 */
	public static   Collection<File> readFiles(Collection<File> files, String[] filter) {
		LinkedList<File> resultList = new LinkedList<File>();
		for (File file:files) {
			if (file.exists()) {
				if (file.isDirectory()) {
					Collection<File> subFiles = new LinkedList<File>();
					Collections.addAll(subFiles, file.listFiles());
					resultList.addAll(readFiles(subFiles, filter));
				} 
				else if(filter.length==0){
					resultList.add(file);
				}				
				else {
					for (int k = 0; k < filter.length; k++) {
						if (file.getName().endsWith(filter[k]))
							resultList.add(file);
					}
				}
			}
		}
		return resultList;
	}

	public static   String[] readFileURLs(Collection<File> files, String[] filter) {
		ArrayList<String> resultURLs = new ArrayList<String>(files.size());
		Collection<File> sourceFiles = readFiles(files, filter);
		for (File file:sourceFiles) {
			 resultURLs.add(file.getAbsolutePath());
		}
		return (String[])resultURLs.toArray();
	}
	
	public static void printString(String fragment, String resourceName, File outputDirectory) throws IOException {
		if(!outputDirectory.isDirectory()){
			throw new IOException("Directory '"+ outputDirectory.toString() +"' is not a directory.");
		}
		String fileName = resourceName;
		File outputFile = new File(outputDirectory, fileName);
		if(!outputFile.getParentFile().exists()){
			outputFile.getParentFile().mkdirs();
		}
		FileWriter writer = new FileWriter(outputFile);
		
		printString(fragment,writer);	
	}
	
	public static void printString(String fragment, String resourceName, File outputDirectory, String encoding) throws IOException {
		if(!outputDirectory.isDirectory()){
			throw new IOException("Directory '"+ outputDirectory.toString() +"' is not a directory.");
		}
		String fileName = resourceName;
		File outputFile = new File(outputDirectory, fileName);
		if(!outputFile.getParentFile().exists()){
			outputFile.getParentFile().mkdirs();
		}
		OutputStream out = new FileOutputStream(outputFile);
		OutputStreamWriter writer = new OutputStreamWriter(out,Charset.forName(encoding));
		
		printString(fragment,writer);
		
	}
	
	public static void printString(String fragment, Writer out) throws IOException{
		out.append(fragment);
		out.flush();
		out.close();
	}

}
