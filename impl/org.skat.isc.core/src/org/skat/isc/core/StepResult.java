/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */
package org.skat.isc.core;

public final class StepResult {	
	public static final StepResult OK = new StepResult(""); 
	public static final StepResult CONTRACT_FAILED = new StepResult("Contract violation.");
	public static final StepResult TYPE_CHECK_FAILED = new StepResult("Syntactic types are not compatible.");
	public static final StepResult AST_CHECK_FAILED = new StepResult("AST structure is not adaquate for composition (e.g, Opt or List as argument).");
	public static final StepResult POINT_CHECK_FAILED = new StepResult("Composer and point type do not fit.");
	public static final StepResult COMBINED_FAILURE = new StepResult("Two or more checks failed.");
	public static final StepResult UNCATEGORIZED_FAILURE = new StepResult("An uncategorized failure occured.");
	public static final StepResult FRAGMENT_MISSING = new StepResult("A source fragment was not available.");
	public static final StepResult COMPOSER_MISSING = new StepResult("A matching composer of the correct type is missing.");
	
	public static StepResult createDetailedResult(StepResult resultGroup, String... details){
		if(resultGroup==null)
			throw new RuntimeException();
		return new StepResult(resultGroup,details);
	}
	
	private StepResult resultGroup = null;
	private String[] details = null;
	
	private StepResult(String msgString){
		details = new String[]{msgString};
	}
	
	private StepResult(StepResult resultGroup, String... details){
		this.resultGroup = resultGroup;
		this.details = details;
	}
	
	public String getMessage(){
		if(resultGroup==null)
			return details[0];
		else{
			String s = resultGroup.getMessage();	
			if(details!=null&&details.length>0){
				s += " Details: ";
				for(int i=0;i<details.length;i++){
					s+=details[i];
				}
			}
			return s;
		}
	}
	
	public StepResult getResultGroup(){
		if(resultGroup==null)
			return this;
		return resultGroup;
	}
	
	public String[] getMessageArray(){
		return details;
	}
}
