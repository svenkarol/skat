/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */
package org.skat.isc.core;

public class StatusMessages {
	
	public static final String PROBLEM_OCCURED = "Problem occured.";
	public static final String SIDE_EFFECT_PROBLEM = "Side effects not realized.";
	public static final String POINT_NOT_FOUND = "Target point not found or exhausted.";
	public static final String NON_COMPOSER_NODE = "Cannot execute complex composition on a non composer node.";
	public static final String NOT_AN_EXTEND = "Composer is not an Extend composer.";
	public static final String NOT_AN_EXTRACT = "Composer is not an Extract composer.";
	public static final String NOT_A_BIND = "Composer is not a Bind composer.";
	public static final String NOT_A_SLOT = "Node is not a slot.";
	public static final String NOT_A_HOOK = "Node is not a hook.";
	public static final String NOT_A_RUDIMENT = "Node is not a rudiment.";
	public static final String FRAGMENT_NOT_FOUND = "A source fragment was not available.";
	public static final String CANNOT_SET_COMPOSER_ARG = "Cannot set optional composer arg on non composer node.";
	public static final String ALLOWED_AST_TYPES = "The extending fragment must be one of the types {#TYPES#} or an adequate subtype.";
}
