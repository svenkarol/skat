/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */
package org.skat.isc.core;

public class StatusDescriptor {
	
	private Category type;
	private String message;
	private Throwable exception = null;
	
	private String pointName = null;
	//private String pointType = null;
	private String fragmentName = null;
	//private String fragmentType = null;
	private String composerType = null;
	
	private StepResult resultType = null;
	
	private StatusDescriptor(){}
	
	public StatusDescriptor(Category type, String message, Throwable exception){
		this.type = type;
		this.message = message;
		this.exception = exception;
	}
	
	public StatusDescriptor(Category type, 
							String message, 
							Throwable exception, 
								String pointName, 
								String fragmentName, 
								String composerType, 
								StepResult result){
		this.type = type;
		this.message = message;
		this.exception = exception;
		this.pointName = pointName;
		this.fragmentName = fragmentName;
		this.composerType = composerType;
		this.resultType = result;
	}

	public StatusDescriptor(Category type, String pointName, String fragmentName, String composerType, StepResult result){
		this.type = type;
		this.pointName = pointName;
		this.fragmentName = fragmentName;
		this.composerType = composerType;
		this.resultType = result;
	}
	
	public Category getProblemType() {
		return type;
	}
	
	public String getMessage() {
		return message;
	}
	
	public Throwable getException() {
		return exception;
	}
	
	public String getComposerType(){
		return composerType;
	}
	
	public String getPointName(){
		return pointName;
	}
	
	public String getFragmentName(){
		return fragmentName;
	}
	
	public StepResult getResultType(){
		return resultType;
	}
		
	public static StatusDescriptor ERROR(String message){
		StatusDescriptor p = new StatusDescriptor();
		p.message = message;
		p.type = Category.ERROR;
		return p;
	}
	
	public static StatusDescriptor ERROR(String message, Throwable exception){
		StatusDescriptor p = new StatusDescriptor();
		p.message = message;
		p.type = Category.ERROR;
		p.exception = exception;
		return p;
	}
	
	public static StatusDescriptor ERROR(String message, Throwable exception,
			String pointName, 
			String fragmentName, 
			String composerType, 
			StepResult result){
		StatusDescriptor p = new StatusDescriptor(
				Category.ERROR,
				message,
				exception,
				pointName,
				fragmentName,
				composerType,
				result
				);
	   return p;
	}
	public static StatusDescriptor WARNING(String message){
		StatusDescriptor p = new StatusDescriptor(
				Category.WARNING,
				message,
				null,
				null,
				null,
				null,
				null
				);		
	return p;
	}
	
	public static StatusDescriptor WARNING(String message,
			String pointName, 
			String fragmentName, 
			String composerType, 
			StepResult result){
		StatusDescriptor p = new StatusDescriptor(
				Category.WARNING,
				message,
				null,
				pointName,
				fragmentName,
				composerType,
				result
				);		
	return p;
	}
	
	public static StatusDescriptor MESSAGE(String message){
		StatusDescriptor p = new StatusDescriptor();
		p.message = message;
		p.type = Category.INFO;
		return p;
	}
	
	public static StatusDescriptor MESSAGE(String message,
			String pointName, 
			String fragmentName, 
			String composerType, 
			StepResult result){
		StatusDescriptor p = new StatusDescriptor(
				Category.INFO,
				message,
				null,
				pointName,
				fragmentName,
				composerType,
				result
				);				
		return p;
	}
	
	public static StatusDescriptor FAILURE(String message,
			String pointName, 
			String fragmentName, 
			String composerType, 
			StepResult result){
		StatusDescriptor p = new StatusDescriptor(
				Category.FAILURE,
				message,
				null,
				pointName,
				fragmentName,
				composerType,
				result
				);				
		return p;
	}
	
	
	public static StatusDescriptor FAILURE(String message, Throwable exception){
		StatusDescriptor p = new StatusDescriptor();
		p.message = message;
		p.type = Category.FAILURE;
		p.exception = exception;
		return p;
	}
	
	public static StatusDescriptor FAILURE(String message, Throwable exception,
			String pointName, 
			String fragmentName, 
			String composerType, 
			StepResult result){
		StatusDescriptor p = new StatusDescriptor(
				Category.FAILURE,
				message,
				exception,
				pointName,
				fragmentName,
				composerType,
				result
				);		
		return p;
	}

	public String toString(){
		String s = "[" + getProblemType().name() + "]" 
			 	+ toSituationString() + ( getMessage()!=null?" " + getMessage():"");
		if(exception!=null){
			s+= "\n Exception Message: ";
			s+= getException().getLocalizedMessage();
		}
		return s;
	}
	
	public String toSituationString(){
		String result = "";
		if(composerType!=null||pointName!=null||fragmentName!=null){
			result = (composerType==null?"?":composerType)
					+ "[" + (fragmentName==null?"?":fragmentName)
							+  "->" + (pointName==null?"?":pointName+"]");
			if(resultType!=null&&resultType!=StepResult.OK){
				result = "Issue with " + result + ": " + resultType.getMessage();
			}
		}
		return result;
	}
	
	/**
	 * Depending on the problem category, gives a hint if processing can continue.
	 * 
	 * @return true, if composition can be continued.
	 */
	public boolean isRecoverable(){
		return type == Category.INFO || type == Category.WARNING || type == Category.ERROR;
	}
	
	/**
	 * Depending on the problem category, gives a hint if processing should not continue.
	 * 
	 * @return true, if it is recommended to stop processing.
	 */
	public boolean shouldStop(){
		return type == Category.FAILURE || type == Category.ERROR;
	}
	
	public static enum Category {
		INFO, WARNING, ERROR, FAILURE;
	}
}