/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */
package org.skat.isc.core;

public class CompositionException extends RuntimeException {

	private static final long serialVersionUID = 165980739633333340L;
	
	
	public CompositionException(String message){
		super(message);
	}
	
	public CompositionException(String message, Throwable cause){
		super(message,cause);
	}
	
}
