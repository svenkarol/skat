/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */


package org.skat.isc.minimal;

import java.io.IOException;

public class CodeGenException extends IOException{

	private static final long serialVersionUID = 1659807396698064740L;
	
	
	public CodeGenException(String message){
		super(message);
	}
	
	public CodeGenException(String message, Throwable cause){
		super(message,cause);
	}
	
}
