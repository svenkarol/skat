/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.upp;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.net.URI;
import java.util.Collection;
import java.util.Collections;

import org.skat.binding.upp.ast.GenericSource;
import org.skat.binding.upp.ast.Box;
import org.skat.binding.upp.ast.Element;
import org.skat.binding.upp.ast.CompositionEnvironment;
import org.skat.binding.upp.ast.GenericFragment;
import org.skat.binding.upp.ast.IfCondition;
import org.skat.binding.upp.ast.List;
import org.skat.binding.upp.ast.CompositionSystem;

import org.skat.binding.upp.UPPParser;

import org.skat.isc.core.StatusDescriptor;
import org.skat.isc.minimal.CodeGenException;

import static org.skat.isc.core.IOHelper.*;

public class UPProcessor {
	
	private CompositionSystem cSys = null;
	
	public UPProcessor(String baseURI, String inURI, String outURI) throws IOException {
		instantiateCompositionSystem(baseURI,inURI,outURI);
	}
	
	private void instantiateCompositionSystem(String baseURI, String inURI, String outURI) throws IOException{
		cSys = new CompositionSystem(baseURI,inURI,outURI){

			@Override
			protected CompositionEnvironment createEnvironment() throws IOException {
				GenericSource genSource = new GenericSource();
				Collection<File> fragmentFiles = readFiles(Collections.singletonList(new File(URI.create(getInURI()))), new String[0]);
				for(File fragmentFile:fragmentFiles){
					String content = readFile(fragmentFile);
					UPPParser parser = new UPPParser(new StringReader(content),fragmentFile.getName());
					GenericFragment fragment = parser.parseGenericFragment();
					fragment.setName(fragmentFile.getName());
					genSource.addFragment(fragment);
				}
				return genSource;
			}

			@Override
			public void doPersist(Box box) throws IOException {
				GenericFragment fragment = (GenericFragment)box;
				String content = fragment.genString();
				printString(content,fragment.resourceName(),new File(URI.create(getOutURI())));
			}
			
		};
	}
		
	public CompositionSystem getCore(){
		return cSys;
	}
	
	public void persist(String regex) throws IOException{
		cSys.persistFragments(regex);
	}
	
	public void persistAll() throws IOException{
		cSys.persistFragments();
	}
	
	public void processMacros(Object model) throws CodeGenException{
		((GenericSource)cSys.getEnv()).setModel(model);
		for(Box fragment:cSys.getEnv().getFragmentList()){
			System.out.println("upp> Processing: " +fragment.getName());
			processGenericElements(((GenericFragment)fragment).getElementsList());
		}
	}
	
	private boolean processGenericElements(List<Element> elements) throws CodeGenException {
		int elementCount = elements.getNumChild(); 
		int i = 0;
		while(i<elementCount){
			Element current = elements.getChild(i);
			if(current.isComposer()){
				if(current instanceof IfCondition){
					IfCondition condition = (IfCondition)current;
					if(!condition.isValid())
						throw new CodeGenException("'If' condition in " + current.owningBox().getName() + " is not of type bool.");					
				}
				current.compose();
				for(StatusDescriptor p:cSys.getEnv().status){
					System.out.println(p);
				}
				elementCount = elements.getNumChild(); 
			}
			else{
				i++;
			}
		}
		
/*		for(int i=0;i<elementCount;i++){
			Element current = elements.getChild(i);
			if(current instanceof Compositional)
				System.out.println("  " + current.getClass().getSimpleName() + ":" +  ((Compositional)current).getName());
				
			if(current.isComposer()){
				System.out.println("    ---> composer");

				if(current instanceof IfCondition){
					IfCondition condition = (IfCondition)current;
					if(!condition.isValid())
						throw new CodeGenException("'If' condition in " + current.owningBox().getName() + " is not of type bool.");					
				}
				if(current.isBind())
					i--;
				if(current instanceof MacroCall){
					MacroCall call = (MacroCall)current;
				}
				current.compose();
				for(CompositionProblem p:cSys.getEnv().compositionProblems){
					System.out.println(p);
				}
				elementCount = elements.getNumChild(); 
			}
		}	*/				
		return true;
	}	
}
