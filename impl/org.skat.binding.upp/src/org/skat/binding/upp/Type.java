/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.upp;

public enum Type {
	LITERAL, INT, BOOL, BOT;
	
	public static String asString(Object value){
		if(value==null)
			return "null";
		else if(value instanceof Integer)
			return ((Integer)value).toString();
		else if(value instanceof Boolean)
			return ((Boolean)value).toString();
		else 
			return value.toString();
	}
	
	public static int asInteger(Object value){
		if(value==null)
			return Integer.MIN_VALUE;
		else if(value instanceof Boolean)
			return (value == Boolean.TRUE)?1:0;			
		else if(value instanceof String)
			return Integer.valueOf((String)value);
		else if(value instanceof Integer)
			return (Integer)value;
		else
			return Integer.valueOf((value.toString()));
	}
	
	public static boolean asBoolean(Object value){
		if(value==null)
			return false;
		else if(value instanceof String)
			return (((String)value).length()==0)?false:true;
		else if(value instanceof Integer)
			return ((Integer)value)>=0?true:false;
		else if(value instanceof Boolean)
			return (Boolean)value;
		else
			return false;
	}
	
	public static Type compAddType(Type left, Type right){
		if(left==Type.BOT||right==Type.BOT)
			return Type.BOT;
		if(left==Type.LITERAL||right==Type.LITERAL)
			return Type.LITERAL;
		else if(left==Type.INT||right==Type.INT){
			return Type.INT;
		}
		else if(left==Type.BOOL && right==Type.BOOL){
			return Type.BOOL;
		}
		else{
			//should not happen
			return Type.BOT;
		}
	}
	
	public static Type compMulType(Type left, Type right){
		if(left==Type.INT && right==Type.INT)
			return Type.INT;
		return Type.BOT;
	}
	
	public Class<?> mapTypeJava(){
		if(this==LITERAL)
			return String.class;
		else if(this==BOOL)
			return boolean.class;
		else if(this==INT)
			return int.class;
		else
			return null;
	}
	
	public static Type mapJavaType(Class<?> javaClass){
		if(javaClass==String.class)
			return LITERAL;
		else if(javaClass==boolean.class)
			return BOOL;
		else if (javaClass==Boolean.class)
			return BOOL;
		else if(javaClass==int.class)
			return INT;
		else if(javaClass==Integer.class)
			return INT;
		return BOT;
	}
}
