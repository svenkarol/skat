/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

import org.skat.binding.upp.Type;

aspect ExpressionEvaluator {
	
	syn lazy Object Expression.value();
	syn lazy Type Expression.type();
	
	//Logical Expressions:
	eq Or.type() = (getLeftArg().type()==Type.BOOL && getRightArg().type()==Type.BOOL)?Type.BOOL:Type.BOT;
	eq Or.value() {
		if(type()==Type.BOOL){
			return ((Boolean)getLeftArg().value()) || ((Boolean)getRightArg().value());
		}
		else
			throw new RuntimeException("Or cannot be applied to types different then BOOL.");
	}

	eq And.type() = (getLeftArg().type()==Type.BOOL && getRightArg().type()==Type.BOOL)?Type.BOOL:Type.BOT;
	eq And.value() {
		if(type()==Type.BOOL){
			return ((Boolean)getLeftArg().value()) && ((Boolean)getRightArg().value());
		}
		else
			throw new RuntimeException("And cannot be applied to types different then BOOL.");
	}

	eq Not.value() {
		if(getArg().type()!=Type.BOOL)
			throw new RuntimeException("Not cannot be applied to type " + getArg().type() + ".");
		return !((Boolean)getArg().value());
	}
	
	eq Not.type() = getArg().type()==Type.BOOL?Type.BOOL:Type.BOT;

	//Comparative Expressions:
	eq LessThen.type() = (getLeftArg().type()==getRightArg().type())?getLeftArg().type():Type.BOT;
	eq LessThen.value() {
		if(type()==Type.BOOL)
			return ((Boolean)getLeftArg().value())==false?getRightArg().value():false;
		else if(type()==Type.LITERAL)
			return ((String) getLeftArg().value()).length() < ((String) getRightArg().value()).length();
		else if(type()==Type.INT)
			return ((Integer) getLeftArg().value()) < ((Integer) getRightArg().value());
		else
			throw new RuntimeException("LT cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");
	}
	
	eq GreaterThen.type() = (getLeftArg().type()==getRightArg().type())?getLeftArg().type():Type.BOT;
	eq GreaterThen.value() {
		if(type()==Type.BOOL)
			return ((Boolean)getLeftArg().value())==true?!((Boolean)getRightArg().value()):false;
		else if(type()==Type.LITERAL)
			return ((String) getLeftArg().value()).length() > ((String) getRightArg().value()).length();
		else if(type()==Type.INT)
			return ((Integer) getLeftArg().value()) > ((Integer) getRightArg().value());
		else
			throw new RuntimeException("GT cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");
	}
	
	eq Equals.type() = (getLeftArg().type()==getRightArg().type())?getLeftArg().type():Type.BOT;
	eq Equals.value() {
		if(type()==Type.BOOL||type()==Type.INT)
			return getLeftArg().value()==getRightArg().value();
		else if(type()==Type.LITERAL)
			return getLeftArg().value().equals(getRightArg().value());
		else
			throw new RuntimeException("Equals cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");
	}
	
	//Arrithmetic Expressions:
	eq Plus.type() = Type.compAddType(getLeftArg().type(),getRightArg().type());

	eq Plus.value() {
		if(type()==Type.LITERAL)
			return Type.asString(getLeftArg().value()) + Type.asString(getRightArg().value());
		else if(type()==Type.INT)
			return Type.asInteger(getLeftArg().value()) + Type.asInteger(getRightArg().value());
		else if(type()==Type.BOOL)
			return Type.asInteger(getLeftArg().value()) + Type.asInteger(getRightArg().value());
		else
			throw new RuntimeException("Plus cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");
	}
	
	eq Minus.type() = Type.compAddType(getLeftArg().type(),getRightArg().type()); 
	
	eq Minus.value() {
		if(type()==Type.LITERAL)
			return Type.asInteger(getLeftArg().value()) - Type.asInteger(getRightArg().value());
		else if(type()==Type.INT)
			return Type.asInteger(getLeftArg().value()) - Type.asInteger(getRightArg().value());
		else if(type()==Type.BOOL)
			return Type.asInteger(getLeftArg().value()) - Type.asInteger(getRightArg().value());
		else
			throw new RuntimeException("Minus cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");	
	}
	
	eq Mult.type() = Type.compMulType(getLeftArg().type(),getRightArg().type());
			
	eq Mult.value() { 
		if(type()==Type.INT)
			return Type.asInteger(getLeftArg().value()) * Type.asInteger(getRightArg().value());
		else
			throw new RuntimeException("Mult cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");	
	}
	
	eq Div.type() = Type.compMulType(getLeftArg().type(),getRightArg().type());

	eq Div.value() { 
		if(type()==Type.INT)
			return Type.asInteger(getLeftArg().value()) / Type.asInteger(getRightArg().value());
		else
			throw new RuntimeException("Mult cannot be applied to arguments of type " + getLeftArg().type() + " and " + getRightArg().type() + ".");	
	}

	
	//Simple Expressions:
	eq Nested.value() = getExpression().value();
	eq Nested.type() = getExpression().type();
	
	eq Constant.value() { 
		if(type()==Type.LITERAL)
			return extract(getStringValue(),"\"","\"");
		else if(type()==Type.INT)
			return Integer.parseInt(getStringValue());
		else if(type()==Type.BOOL)
			return Boolean.parseBoolean(getStringValue());
		throw 
			new RuntimeException("Wrong Type " + type() + " of constant value " + getStringValue() + ".");
	}
	
	eq Constant.type() {
		return getType();
	}
	   
	eq Defined.value() = getReference().macro()==null?false:true;
	eq Defined.type() = Type.BOOL;	

	//Model call boiler plates
	eq Call.value() {
		if(type()==Type.BOT)
			throw new RuntimeException("Type of call expression of model operation " + getOpName() + " could not be evaluated, check call signature and model.");
		Object model = computationModel();
		if(model!=null){
			Class<?> modelType = model.getClass();
			try{
				java.lang.reflect.Method method = modelType.getMethod(getOpName(),javaArgSigs());		
				Object[] args = argValues();
				return method.invoke(model, args);
			}
			catch (Exception e){ throw new RuntimeException("Could not invoke model operation " + getOpName() + ". Reason: ["+ e.getMessage() +"].",e);}
		}
		return null;
	}
	
	eq Call.type() {   	
		Object model = computationModel();
		if(model!=null){
			Class<?> modelType = model.getClass();
			Class<?>[] argTypes = javaArgSigs();
			if(argTypes!=null) try {
				java.lang.reflect.Method method = modelType.getMethod(getOpName(), argTypes);
				return Type.mapJavaType(method.getReturnType());
			} 
			catch (Exception e) {}			
		}
		return Type.BOT;
	}
	
	syn Object[] Call.argValues();
	
	eq Call.argValues() {
		int listChilds = getArgsList().getNumChild();
		Object[] values = new Object[listChilds];
		if(type()!=Type.BOT){
			for(int i=0;i<listChilds;i++){
				Expression exp = getArgsList().getChild(i);	
				values[i] = exp.value();
			}
		}
		return values;
	}
	
	syn lazy Class<?>[] Call.javaArgSigs();
	
	eq Call.javaArgSigs() {
		Class<?>[] argTypes = new Class[getArgsList().getNumChild()];
		for(int i=0;i<getArgsList().getNumChild();i++){
			Expression exp = getArgsList().getChild(i);	
			Type expType = exp.type();
			if(expType==Type.BOT||expType.mapTypeJava()==null){
				return null;
			}
			argTypes[i] = expType.mapTypeJava();
		}
		return argTypes;
	}
 }

aspect ModelConfiguration {
	inh Object ASTNode.computationModel();
	
	eq GenericSource.getFragment(int index).computationModel() {
		return getModel();
	}
	private Object GenericSource.model = null; 
	public void GenericSource.setModel(Object model){
		this.model = model;
	}
	public Object GenericSource.getModel(){
		return model;
	}
}