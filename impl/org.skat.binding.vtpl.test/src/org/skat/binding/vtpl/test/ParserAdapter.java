/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl.test;

import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

import org.skat.binding.vtpl.VtplParser;

import xtc.parser.ParseError;
import xtc.parser.Result;

public class ParserAdapter {
	
	private VtplParser delegatee = null;
	
	public ParserAdapter(InputStream in){
		delegatee = new VtplParser(in);
	}
	
	public ParserAdapter(Reader in, String resourceName){
		delegatee = new VtplParser(in,resourceName);
	}
	
	public Object parse() throws IOException{
		final Result result = delegatee.pGenericFragmentDecl(0);
		if(result instanceof ParseError){
			return new ParsingError() {
				
				@Override
				public String getMessage() {
					return "ERROR:"+result.parseError().msg+"("+delegatee.location(result.index).line+","+delegatee.location(result.index).column+")";
				}
			};
		}
		return result.semanticValue();
	}
	
}
