/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl.test;
 
import static org.junit.Assert.*;

import static org.skat.isc.core.IOHelper.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;
import org.skat.binding.vtpl.ast.ASTNode;
import org.skat.isc.minimal.CodeGenException;
import org.skat.binding.vtpl.VTPLTemplate;

public class TemplateEngineTest {
	
	
	@Test
	public void testSlotBinding() throws IOException{
		String fileName = "./testdata/engine/Slots.jrag";
		String resultFileName = "./testdata/engine/Slots_result.jrag";
		VTPLTemplate template = new VTPLTemplate("http://my.location",readFile(new File(fileName)));
		template.bind("LISTTYPE","ASTList");
		template.bind("OPTTYPE_WRONG","ASTOpt");
		template.bind("OPTTYPE","ASTOpt");
		template.bind("ASTNode_stuffName", "MyStuff");
		assertNotNull(template.getEnv().findSlot("TOSTRING.*.PFX"));
		template.bind("TOSTRING.REPRESENTATION.PFX", "\"[REPRESENTATION] \"");
		template.bind("TOSTRING.*.PFX", "\"[STANDARD] \"");
		template.extractVariant("TOSTRING", "STANDARD");
		String expectedResultCode = readFile(new File(resultFileName));
		assertFalse(template.hasPoints());
		assertEquals(expectedResultCode,template.getGenCode());
	}
	
	@Test
	public void testQueries() throws FileNotFoundException, CodeGenException{
		assertTrue("SLOT_NAME".matches(ASTNode.convertQueryToRegex("SLOT_NAME")));
		assertTrue("SLOT_NAME".matches(ASTNode.convertQueryToRegex("*.SLOT_NAME")));
		assertTrue("PREFIX1.PREFIX2.SLOT_NAME".matches(ASTNode.convertQueryToRegex("*.SLOT_NAME")));
		assertTrue("PREFIX1.PREFIX2.SLOT_NAME".matches(ASTNode.convertQueryToRegex("PREFIX1.PREFIX2.SLOT_NAME")));		
		assertTrue("PREFIX1.PREFIX2.SLOT_NAME".matches(ASTNode.convertQueryToRegex("PREFIX1.*.SLOT_NAME")));		
		assertTrue("SLOT_NAME".matches(ASTNode.convertQueryToRegex("*.*.SLOT_NAME")));	
		assertFalse("aSLOT_NAME".matches(ASTNode.convertQueryToRegex("*.*.SLOT_NAME")));
		assertFalse("PREFIX.aSLOT_NAME".matches(ASTNode.convertQueryToRegex("SLOT_NAME")));
		assertFalse("PREFIX.aSLOT_NAME".matches(ASTNode.convertQueryToRegex("*.*.SLOT_NAME")));
	}
	
	@Test
	public void testVariantsSelection() throws IOException{
		String fileName = "./testdata/engine/Variants.jrag";
		String resultFileName = "./testdata/engine/Variants_result_1.jrag";
		VTPLTemplate template = new VTPLTemplate("http://my.location",readFile(new File(fileName)));
		assertTrue("template has no variants",template.hasVariants());
		//debug:
		/*for(ASTNode node:template.getEnvRoot().collVariants()){
			System.out.println(node.pointPrefix()+"."+((Compositional)node).pointName());
			System.out.println(node.hasQName(new QRef(((Compositional)node).pointName())));
		}*/
		assertTrue(template.hasVariant("TOSTRING"));
		//template.extractVariant("TOSTRING", "STANDARD");
		//System.out.println(template.getEnv().compositionProblems..getMessage());
		assertTrue(template.extractVariant("TOSTRING", "STANDARD"));
		
		assertTrue(template.extractVariant("CIRCULAR", "NoneOfBoth"));
		assertFalse(template.extractVariant("NESTED","VARIANT2"));
		assertTrue(template.bind("blub","test2;"));
		assertTrue(template.removeFirst("NESTED"));
		assertTrue(template.hasPoint("SomeSlot"));
		String expectedResultCode = readFile(new File(resultFileName));
		assertEquals(expectedResultCode,template.getGenCode());
	}
	
	@Test
	public void testPrototypeInstanciation() throws IOException{
		String fileName = "./testdata/engine/Prototypes.jrag";
		String resultFileName = "./testdata/engine/Prototypes_result_1.jrag";
		VTPLTemplate template = new VTPLTemplate("http://my.location",readFile(new File(fileName)));
		assertTrue(template.hasPoint("DURINGRESET.ASPECTNAME"));
		assertTrue(template.instanciatePrototype("DURINGRESET","DURINGRESET.ASPECTNAME","testMe3"));		
		assertTrue(template.instanciatePrototype("DURINGRESET","DURINGRESET.ASPECTNAME","testMe4"));
		assertTrue(template.removePrototype("DURINGRESET"));
		assertFalse(template.hasPoint("ASPECTNAME"));
		assertNotNull(template.hasPoint("DURINGRESET"));
		assertTrue(template.instanciatePrototype("DURINGRESET"));	
		assertTrue(template.instanciatePrototype("DURINGRESET"));	
		assertTrue(template.removePrototype("DURINGRESET"));
		assertTrue(template.extractVariant("TEST","A"));
		System.out.println(template.getGenCode());
		File resultFile = new File(resultFileName);
		assertTrue(resultFile.exists());
		String expectedResultCode = readFile(resultFile);
		assertEquals(expectedResultCode,template.getGenCode());
	}

}
