/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl.test.mockup;

public class AggregateComponents extends Components{
	String name;
	String type;
	public AggregateComponents(String name, String typeName){
		this.name = name;
		this.type = typeName;
	}
	public String name(){return name;}	
	public String constrParmType(){return type;}
	public boolean isNTA(){return false;}
}