/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl.test.mockup;

import java.util.Iterator;
import java.util.LinkedList;


//ASTDecl Mockup
public class PersonASTDecl {
	public String name(){return "Person";}
	public String getFileName(){return "BAF.ast";}
	public String getStartLine(){return "42";}
	public Iterator<Components> getComponents(){
		LinkedList<Components> components = new LinkedList<Components>();
		components.add(new AggregateComponents("name", "Name"));
		components.add(new TokenComponent("age", "int"));
		return components.iterator();
	}
}