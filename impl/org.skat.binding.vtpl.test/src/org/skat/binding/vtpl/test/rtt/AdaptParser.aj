package org.skat.binding.vtpl.test.rtt;

import org.skat.binding.vtpl.test.ParserAdapter;

import rtt.annotations.*;


public privileged aspect AdaptParser {
	// Specify parser class:
	declare @type: ParserAdapter : @Parser;
	
	// Insert AST access method:
	declare @method:
	    public Object ParserAdapter.parse() : @Parser.AST;
	

}
