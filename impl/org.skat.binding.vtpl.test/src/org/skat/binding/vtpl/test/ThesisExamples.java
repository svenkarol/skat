/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl.test;
 
import static org.junit.Assert.*;

import static org.skat.isc.core.IOHelper.*;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import org.junit.Test;

import org.skat.binding.vtpl.VTPLTemplate;
import org.skat.binding.vtpl.test.mockup.AggregateComponents;
import org.skat.binding.vtpl.test.mockup.Components;
import org.skat.binding.vtpl.test.mockup.PersonASTDecl;
import org.skat.binding.vtpl.test.mockup.TokenComponent;

public class ThesisExamples extends PersonASTDecl {
	
	@Test
	public void testConstructorSlotExample() throws IOException{
		String specURI = "./testdata/thesis/Constructors_Slots.jadd";
		String resultFileName = "./testdata/thesis/Constructors_Slots_result.jadd";
		//PersonASTDecl decl = new PersonASTDecl();
		String content = readFile(new File(specURI));
		//TPLUtil.readStream(getClass().getResourceAsStream(specURI));
		VTPLTemplate tpl = new VTPLTemplate("http://my.location",content);
		tpl.bind("ID", name());
		tpl.bind("FILENAME",getFileName());
		tpl.bind("LINE",String.valueOf(getStartLine()));
		tpl.bind("FINIT","is$Final(true);");
		String result = tpl.getGenCode();
	
		File resultFile = new File(resultFileName);
		assertTrue(resultFile.exists());
		String expectedResultCode = readFile(resultFile);
		assertEquals(expectedResultCode,result);
	}
	
	@Test
	public void testConstructorVariantsPrototypesExample() throws IOException{
		String specURI = "./testdata/thesis/Constructors_PT_VL.jadd";
		String resultFileName = "./testdata/thesis/Constructors_PT_VL_result.jadd";
		String content = readFile(new File(specURI));
		//TPLUtil.readStream(getClass().getResourceAsStream(specURI));
		VTPLTemplate tpl = new VTPLTemplate("http://my.location",content);
		@SuppressWarnings("rawtypes")
		int index = 0;
		String sep = "";
		for(Iterator it = getComponents(); it.hasNext();){
			Components component = (Components) it.next();
			tpl.instanciatePrototype("SETCHILDVARIANTS");
			System.out.println(tpl.getGenCode());
			//tpl.getEnv()
			assertNotNull(tpl.getEnv().findSlot("SETCHILD.*.IDX"));
			System.out.println(tpl.bind("SETCHILD.*.IDX",index));
			if(!component.isNTA()){
				tpl.instanciatePrototype("ARGLIST", "ARGLIST.ARGTYPE",sep+component.constrParmType(),"ARGLIST.IDX",String.valueOf(index));
				sep = ", ";
			}
			if(component instanceof TokenComponent){
				tpl.extractVariant("SETCHILD", "TERMINAL");
				tpl.bind("ARG",component.name());
			} else if(component instanceof AggregateComponents){
				if(!component.isNTA()){
					tpl.extractVariant("SETCHILD", "NONTERMINAL");
				} else{
					tpl.extractVariant("SETCHILD", "NTA2");
				}
			}
			/*else if(component instanceof ListComponents||component instanceof OptionalComponent) {
			    String consName = component instanceof ListComponents?ASTNode.listName:ASTNode.optName;
				tpl.instanciatePrototype("SETCHILD","SETCHILD.CONS",consName,"SETCHILD.IDX",String.valueOf(index));
				if(!component.isNTA()){
					tpl.extractVariant("SETCHILD", "NONTERMINAL");
				}
				else{
					tpl.extractVariant("SETCHILD", "NTA1");
					tpl.bind("CONS",consName);
				}
			}*/
			System.out.println(tpl.getGenCode());
			index++;
		}
		tpl.removePrototype("SETCHILDVARIANTS");
		tpl.removePrototype("ARGLIST");
		tpl.removePrototype("SETCHILD");
		System.out.println(tpl.getGenCode());
	}
	

}
