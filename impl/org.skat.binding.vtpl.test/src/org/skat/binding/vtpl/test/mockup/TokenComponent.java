/* 
 * Copyright (c) 2012-2014 
 * Software Technology Group, TU Dresden, Germany
 * All rights reserved. 
 * 
 * Skeletons and Application Templates (SkAT) is covered by the BSD License. 
 * A copy of the full license text can be found in the license folder of this 
 * project.
 * 
 * Contributors:
 * 		Sven Karol - initial API and implementation
 */

package org.skat.binding.vtpl.test.mockup;

public class TokenComponent extends Components{
	TokenID id;
	String type;
	public TokenComponent(String id, String typeName){
		this.id = new TokenID(id);
		this.type = typeName;
	}
	public TokenID getTokenId(){return id;}
	public String constrParmType(){return type;}
	public boolean isNTA(){return false;}
	public String name() {return getTokenId().getID();}
}