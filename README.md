# README #
SkAT (**Sk**eletons and **A**pplication **T**emplates) is a set of tools that allows users to create template languages, template-based code generators and aspect weavers. It builds upon the model of **"invasive software composition (ISC)"** for fragment composition and uses "reference attribute grammars (RAGs)" to specify languages and composition systems. The basic implementation is based on Java and the JastAdd metacompiler or RAGs (www.jastadd.org).

## SETUP ##
To use SkAT, it has to be cloned from the GIT repository: `clone git@bitbucket.org:svenkarol/skat.git`. 

The repository contains a set of Java folders (configured as Eclipse projects)

* `impl/org.skat.isc.core` contains the basic RAG specifications in the specifications subfolder. These are still language indpendent and just imported by all the derived projects like Java, VTPL, Minimal and UPP.

* `impl/org.skat.isc.full` extends the core project and adds a rewrite layer on top of the basic composition operators in the core project.

* `impl/org.skat.isc.binding.java` The **SkAT4J** -- a composition library for Java -- is contained in the "org.skat.isc.binding.java" project. It depends on the full and the core project. You can look into the specifications / semantics / folder. There you find for example the Slots.jrag and Hooks.jrag which contain the slot and hook determination attributes. The Java RAG system is contained in the include folder. You can have a look if you want, its a really big project.

     * SkAT requires a recent version of ANT (http://ant.apache.org/) to build, so make sure that this is properly installed and in the PATH.

     * To build SkAT4J, move to the source directory `cd skat/impl/org.skat.binding.java/`.

     * Type `ant` -- this should build the `skat4j.jar` library.

     * Examples of how to use the library and what you can do with it are contained in the test project `impl/org.skat.isc.binding.java/test`.

 * `impl/org.skat.isc.minimal` extends the core project with an extremely minimalist composition system which only supports variation points (slots).
 
 * `impl/org.skat.isc.binding.vtpl` extends the minimal slot system with a notion for variability markup -- the *Variability Template Language (VTpL)*.

 * `impl/org.skat.isc.binding.upp` extends the minimal slot system with a C-preprocessor like syntax for macro definition and expansion -- the *Universal Extensible Preprocessor (UPP)*
 
 * `impl/org.skat.isc.binding.fortran` experimentally extends the minimal slot system with a support for small portion of Fortran 2008 code to show how even the simplest macro system can use program analyses.

## DOCUMENTATION ##
 You can find some more information on SkAT in the [SkAT Wiki](https://bitbucket.org/svenkarol/skat/wiki/Home) at BitBucket.

## LICENSE ##
The code written in context of the project is published under the modified BSD license (redistributed libraries use BSD, or Apache licenses too). However, there is one exception: parts of the project use the Rats! parser generator which is licensed under GPLv2. If you use these parts, the aggregated application is under GPLv2. If you do not use these parts, the application is BSD.